# *Installation Guide*

This convenient and brief instructional will in no complicated terms guide you through a smooth and flawless setup.

> Note: This is only a spare-time project. There are far more attractive and overall stable platforms on the market.

## Installing the dependencies

Simply run the `setup.sh` file by typing 

``` bash
chmod +x setup.sh && ./setup.sh
```

From here on out follow the displayed instructions.

## Securing the server

> If you already own a certificate skip to "Storing the certificate"

Website owners these days are encouraged to incorporate encryption schemes into their platforms.

### Generating a TLS certificate (HTTPS)

For a comprehensive introduction please visit: [Self-Signed Certificates](https://www.linode.com/docs/security/ssl/create-a-self-signed-tls-certificate/)

> All self-signed certificates won't be trusted by browsers. This is not an issue as all modern browsers allow you to bypass this warning.
> It isn't an issue, just another deficit of it being a side-project.

### Storing the certificate

Place the certificate under your chosen filename in the folder named **"Certificates"**.

# Documentation 

## Realm Philosophy

In an effort to spice things up with boring, repetitive and lifeless terminology there is a medieval spin to given texts.
E.g.
Each "*realm*" is occupied by different "*stores*" which can be perused by various "*users*".

 

## Database Schema

Here's how the postgresql database is structured:

``` mermaid
graph LR
A[Token] --> B[User]
A[Token] --> B[User]
A[Token] --> B[User]
B[User] --> C[Realm]
C[Realm] --> D[RealmHasStore]
C[Realm] --> D[RealmHasStore]
C[Realm] --> D[RealmHasStore]
D[RealmHasStore] --> E[Store]
D[RealmHasStore] --> E[Store]
D[RealmHasStore] --> E[Store]
```

## Login procedure

``` mermaid
sequenceDiagram
Note right of Browser: Request Session<br>With "Guest"
Browser ->> Webserver: Transmit credentials
Webserver->> Database: Does "Guest" exist?
Database->> Webserver: Guest is authorised.
Note right of Webserver: Unique identifier<br>(token) issued.
Webserver->> Database: Store token
Webserver->> Browser: Save token as cookie
Browser--> Webserver: Session initiated
```

After the session's been established all the client will have to transmit is the token value, which won't expose the user credentials.

## Logout procedure

This closely resembles the login scenario, just with a deletion being requested.
 

``` mermaid
sequenceDiagram
Note right of Browser: Orders Server to<br>release "Guest"
Browser ->> Webserver: Transmit token
Webserver->> Database: Does "Guest" exist?
Note right of Webserver: Terminates<br>"Guest" session.
Webserver->> Browser: Confirm logout
Note left of Webserver: Deletes Cookie
Browser--> Webserver: Session released
```

## Sources

Markdown editor: [Stackedit.io](https://stackedit.io/app#)

Postgresql npm package: [pg](https://www.npmjs.com/package/pg)

# Pending: 

* Sessions with timeout capabilities

