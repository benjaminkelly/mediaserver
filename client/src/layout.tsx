import { Fragment } from 'react';
import Header from './partials/header';
import Footer from './partials/footer';
import SocketHandler from './partials/SocketHandler';

declare interface IProps {
	hideHeader?: boolean;
	allowSocket?: boolean;
	children: JSX.Element;
}

export default function Layout(props: IProps) {
	const { allowSocket, hideHeader, children } = props;
	return (
		<Fragment>
			{allowSocket ?
				<SocketHandler />
				: null
			}
			{hideHeader ?
				null
				: <Header />
			}
			{ children }
			<Footer />
		</Fragment>
	);
}
