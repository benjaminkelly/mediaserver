import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from "react-redux";
import store from "./redux/store";
import { ThemeProvider, createTheme } from '@material-ui/core/styles';
import { green, purple } from "@material-ui/core/colors";
import './stylesheets/styles.css';
import 'animate.css';
import './stylesheets/backgroundTransition.min.css';
import 'video.js/dist/video-js.min.css';
import 'jquery/dist/jquery.min';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import 'vanillatoasts/vanillatoasts.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const theme = createTheme({
	palette: {
		primary: purple,
		secondary: green
	}
});

ReactDOM.render(
	<React.StrictMode>
		<Provider store={ store }>
			<ThemeProvider theme={ theme }>
				<ToastContainer autoClose={false} theme={"colored"} toastStyle={{
					borderRadius: '10px',
					width: '320px',
					padding: '20px 17px',
					boxShadow: '1px 1px 3px rgba(0, 0, 0, 0.1)'
				}} />
				<App/>
			</ThemeProvider>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// @ts-ignore
module.hot.accept();
