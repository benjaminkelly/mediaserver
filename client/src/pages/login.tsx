import axios from 'axios';
import {useEffect, useState, Fragment} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {Button, Card, Form, Container, Row, Col} from 'react-bootstrap';
import VanillaToasts from 'vanillatoasts';
import {CrumbEndpoint, JoinEndpoint} from "../Endpoints";
import {Answer} from "../models/Answer";

function LoginPage() {

    const [register, setRegister] = useState(false);
    const [crumb, setCrumb] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [email, setEmail] = useState('');

    const navigate = useNavigate();

    useEffect(() => {
        fetchCrumb();
    }, []);

    const displayLoginFailure = (msg: string) => {
        VanillaToasts.create({
            title: 'Unable to sign in',
            text: msg,
            type: 'error',
            timeout: 2500
        });
    }

    const login = async () => {
        try {
            if (username.trim() === "") {
                displayLoginFailure("Please enter a username.");
            }
            const response = await axios.post<Answer<any>>(JoinEndpoint, {username, password});
            if (response.data && response.data.data) {
                navigate('/gallery', {replace: false});
            } else if (response.data.comment) {
                displayLoginFailure(response.data.comment);
            } else {
                displayLoginFailure("The server is saying something odd.");
            }
        } catch (e) {
            displayLoginFailure(e.message);
        }
    }

    const fetchCrumb = () => {
        axios.get(CrumbEndpoint)
            .then((response) => {
                if (response.data.crumb) {
                    setCrumb(response.data.crumb);
                }
            })
    }

    return (
        <Container className="vh-100 vw-100">
            <Row className="justify-content-center align-items-center vh-100">
                <Col>
                    <Card>
                        <Card.Header>
                            <Card.Title>
                                {(register) ?
                                    "Register" :
                                    "Sign in"
                                }
                            </Card.Title>
                        </Card.Header>
                        {
                            (register) ?
                                <Fragment>
                                    <Form.Control type="hidden" name="crumb" value={crumb}/>
                                    <Form.Group>
                                        <Form.Control placeholder="username"
                                                      onChange={(e) => setUsername(e.target.value)}
                                                      value={username} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control type="password" onChange={(e) => setPassword(e.target.value)}
                                                      value={password} placeholder="password"/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control type="password"
                                                      onChange={(e) => setPasswordConfirm(e.target.value)}
                                                      value={passwordConfirm}
                                                      placeholder="Confirm Password"/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control type="email" onChange={(e) => setEmail(e.target.value)}
                                                      value={email}
                                                      placeholder="email address" required/>
                                    </Form.Group>
                                    <Button variant="success" className="text-uppercase">create</Button>
                                    <p className="message">Already registered? <Button
                                        onClick={() => setRegister(false)}
                                        className="badge badge-secondary">Sign
                                        In</Button></p>
                                </Fragment> :
                                <Fragment>
                                    <Form.Control type="hidden" name="crumb" value={crumb}/>
                                    <Form.Group>
                                        <Form.Control onChange={(e) => setUsername(e.target.value)}
                                                      value={username}
                                                      placeholder="username" required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Control onChange={(e) => setPassword(e.target.value)}
                                                      value={password}
                                                      type="password" id="password"
                                                      placeholder="password"/>
                                    </Form.Group>
                                    <p>Forgot a detail? <Link to="/reset">Reset Password</Link></p>
                                    <Button variant="success" className="text-uppercase"
                                            onClick={login}>login</Button>
                                    <p className="message">Not registered? <Button
                                        onClick={() => setRegister(true)}
                                        className="badge badge-secondary">Create an account</Button></p>
                                </Fragment>
                        }
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default LoginPage;