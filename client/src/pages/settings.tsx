import { Button, MenuItem, TextField, InputAdornment, Container } from '@material-ui/core';
import { MdAccountCircle } from 'react-icons/md';
import axios from 'axios';
import Vanillatoasts from 'vanillatoasts';
import { FormEvent, useEffect, useState } from 'react';
import { RealmEndpoint, UserEndpoint } from '../Endpoints';
import { Realm } from '../models/Realm';
import { User } from '../models/User';
import { toast } from 'react-toastify';
import Toast from '../partials/Card/Toast';
import PasswordPrompt from '../partials/Prompt/PasswordPrompt';
import {useNavigate} from "react-router-dom";

function SettingsPage() {

    const [id, setId] = useState(-1);
    const [name, setName] = useState("");
    const [realmId, setRealmId] = useState(-1);
    const [realms, setRealms] = useState<Realm[]>([]);
    const [userVerified, setUserVerified] = useState(false);
    const [passwordPromptOpen, setPasswordPromptOpen] = useState(false);

    const navigate = useNavigate();

    function displayErrorAndLeave(error) {
        Vanillatoasts.create({
            title: 'Error',
            text: error.toString(),
            type: 'error'
        });
        navigate("/login", {
            replace: true
        });
    }

    useEffect(() => {
        function displayErrorAndLeave(error) {
            Vanillatoasts.create({
                title: 'Error',
                text: error.toString(),
                type: 'error'
            });
            navigate("/login", {
                replace: true
            });
        }
        async function getUser() {
            const { status, data: user } = await axios.get<User>(UserEndpoint);
            if (status === 200 && user) {
                setId(user.id);
                setName(user.name);
                setRealmId(user.realmId);
                setUserVerified(user.verified);
            }
        }
        async function getRealms() {
            const { status, data: realms } = await axios.get<Realm[]>(RealmEndpoint);
            if (status === 200 && realms) {
                setRealms(realms);
            }
        }
        getUser().then(getRealms).catch(displayErrorAndLeave);
    }, [navigate]);

    const changePassword = async (password: string) => {
        setPasswordPromptOpen(false);
        try {
            await axios.patch(UserEndpoint, { id, password });
            toast.success(<Toast title={"Done"} content={"Your password has been updated."} />, {
                position: toast.POSITION.TOP_CENTER
            });
        } catch (e) {
            displayErrorAndLeave(e);
        }
    }

    const onSubmit = async (formEvent: FormEvent<HTMLFormElement>) => {
        formEvent.preventDefault();
        formEvent.stopPropagation();
        try {
            await axios.patch(UserEndpoint, { id, name });
            toast.success(<Toast title={"Done"} content={"Everything is up to date now."} />, {
                position: toast.POSITION.TOP_CENTER
            });
        } catch (e) {
            displayErrorAndLeave(e);
        }
    }

    return (
        <Container maxWidth="sm">
            <PasswordPrompt open={passwordPromptOpen} onClose={() => setPasswordPromptOpen(false)} onceDone={changePassword} />
        <form onSubmit={(formEvent) => onSubmit(formEvent)}>
            <p>Is user verified: {userVerified.toString()} </p>
            <TextField InputProps={{
                startAdornment: (<InputAdornment position="start">
                    <MdAccountCircle />
                </InputAdornment>)
            }} id="username" label="Username" value={name} onChange={(e) => setName(e.target.value)} />
            <br/>
                <Button variant="contained" onClick={() => setPasswordPromptOpen(true)}>Change Password</Button>
                <br/>
            {/* @ts-ignore */}
            <TextField id="realm" label="Realm" select value={realmId === -1 ? '' : realmId} onChange={(e) => setRealmId(e.target.value)}>
                {
                    realms.map((realm, realmKey) => (
                        <MenuItem key={realmKey} value={realm.id}>{realm.name}</MenuItem>
                    ))
                }
            </TextField>
            <br/>
            <Button type="submit" variant="outlined">Submit</Button>
        </form>
        </Container>
    )
}

export default SettingsPage;