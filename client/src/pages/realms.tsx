import { useEffect, useState } from 'react';
import axios from 'axios';
import { Realm } from "../models/Realm";
import { Card } from '@material-ui/core';
import VanillaToasts from 'vanillatoasts';
import { RealmEndpoint } from "../Endpoints";
import {useNavigate} from "react-router-dom";

function RealmView() {

    const [realms, setRealms] = useState<Realm[]>([]);

    const navigate = useNavigate();


    useEffect(() => {
        axios.get<Realm[]>(RealmEndpoint).then((response) => {
            setRealms(response.data);
        }).catch(() => {
            navigate('/login', { replace: true });
        });
    }, [navigate]);

    const selectRealm = async (realm: Realm) => {
        let password: string | null = null;
        if (realm.protected) {
            password = prompt("Enter the password for: " + realm.name, "");
        }
        await axios.put(RealmEndpoint, {
            realmId: realm.id,
            password
        }).then(() => {
            navigate("/gallery", { replace: true });
        }).catch(() => {
            VanillaToasts.create({
                title: "Failed",
                text: "Incorrect password",
                type: "error"
            });
        });
    }

    return (
        (realms.length > 0) ?
            <div id="realm-view">
                <h3>Please select one of the following:</h3>
                {
                    realms.map((realm, key) =>
                        <Card key={key} className="realm-card" id={`${realm.id}`}
                            onClick={() => selectRealm(realm)}>
                            <div className="view overlay">
                                <img className="card-img-top" alt="" src="#" />
                                <div className="card-body">
                                    <h5 className="card-title">{realm.name}</h5>
                                    <p className="card-text">{realm.description}</p>
                                </div>
                                <div className="mask rgba-white-slight"/>
                            </div>
                        </Card>
                    )
                }
            </div>
            :
            <div id="void">
                It's an abyss.
            </div>
    )
}

export default RealmView;
