import axios from 'axios';
import 'react-dropzone-uploader/dist/styles.css';
import Dropzone, {IUploadParams, IFileWithMeta, StatusValue} from 'react-dropzone-uploader';
import '../import-jquery';
import React, {useEffect, useState} from 'react';
import {Col, Row} from 'react-bootstrap';
import 'vanillatoasts/vanillatoasts.css';
import VanillaToasts from 'vanillatoasts';
import {toast} from 'react-toastify';
import {Select, MenuItem, Button, Container, FormGroup, TextField} from '@material-ui/core';
import StoreView from "../partials/gallery/StoreView";
import _ from 'lodash';
import {connect, ConnectedProps} from "react-redux";
import {fetchLibrary, fetchCategories, fetchStores} from "../redux/actions";
import {PulseLoader} from "react-spinners";
import {IReduxStore} from '../redux/reducers';
import {Category} from "../models/Category";
import {CrumbEndpoint, DownloadEndpoint, UploadEndpoint} from "../Endpoints";
import Toast from '../partials/Card/Toast';
import {useNavigate} from "react-router-dom";
import {useLocation} from "react-router";
import isURL from "validator/lib/isURL";

axios.defaults.withCredentials = true;

const mapStateToProps = (store: IReduxStore) => {
    return {
        library: store.library,
        stores: store.stores,
        categories: store.categories
    }
}

const mapDispatchToProps = {
    fetchStores,
    fetchCategories,
    fetchLibrary
}

const connector = connect(mapStateToProps, mapDispatchToProps);

type ReduxProps = ConnectedProps<typeof connector>;

function GalleryPage(props: ReduxProps) {

    const [validSession, setValidSession] = useState<boolean>(true);
    const [crumb, setCrumb] = useState<string>('');
    const [activeCategory, setActiveCategory] = useState<number>(0);
    const [activeStore, setActiveStore] = useState<number>(-1);
    const [downloadUrl, setDownloadUrl] = useState<string>('');
    const [isFetching, setIsFetching] = useState<boolean>(true);

    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        axios.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            if (error.response) {
                if (error.response.status === 401) {
                    setValidSession(false);
                }
            }
            return error;
        });
    }, []);

    const {library, stores, categories, fetchLibrary, fetchStores, fetchCategories} = props;

    useEffect(() => {
        if (!validSession) {
            toast.error(<Toast title={"Session expired"} content={"You have been logged out"}/>);
            VanillaToasts.create({
                title: 'Session expired',
                text: 'You have been logged out',
                type: 'error'
            });
            navigate("/login", {
                replace: true
            });
        }
    }, [validSession, navigate]);

    useEffect(() => {
        axios.get(CrumbEndpoint)
            .then((response) => {
                if (response.data.crumb) {
                    setCrumb(response.data.crumb);
                }
            }).then(async () => {
            setIsFetching(true);
            await fetchCategories();
            await fetchStores();
            await fetchLibrary();
            setIsFetching(false);
        });
    }, [fetchLibrary, fetchCategories, fetchStores]);

    useEffect(() => {
        let selectedCategory: Category = {id: 0, title: 'Loading'};
        try {
            if (categories.length > 0) {
                selectedCategory = categories[0];
            }
            let urlParams = new URLSearchParams(location.search);
            let queryCategory = urlParams.get("category");
            if (queryCategory != null) {
                let parsedCategory = categories.filter((category) => category.title.toLowerCase() === queryCategory?.toLowerCase());
                if (parsedCategory.length === 1) {
                    selectedCategory = parsedCategory[0];
                }
            }
            if (stores.length > 0) {
                setActiveStore(stores[Math.floor(stores.length / 2)].id);
            }
            let categoryIndex = categories.findIndex((category) => category.title === selectedCategory.title);
            if (categoryIndex !== -1) {
                setActiveCategory(selectedCategory.id);
            }
        } catch (e) {
            setIsFetching(false);
            setValidSession(false);
        }
    }, [categories, location, stores]);


    function openCategory(category: Category) {
        setActiveCategory(category.id);
        navigate("/?category=" + category.title, {
            replace: true
        });
    }

    function onDropzoneStatusChange(file, status: StatusValue) {
        if (status === "done") {
            file.remove();
        }
    }

    function getUploadParams(fileWithMeta: IFileWithMeta): IUploadParams {
        const body = new FormData();
        body.append('file', fileWithMeta.file);
        body.append("storeId", activeStore.toString());
        body.append('crumb', crumb);
        // @ts-ignore
        window.body = body;
        return {url: UploadEndpoint, body}
    }

    async function demandDownload(event: React.KeyboardEvent) {
        event.persist();
        if (event.key === 'Enter') {
            if (!isURL(downloadUrl)) {
                return;
            }
            setDownloadUrl('');
            try {
                await axios.post(DownloadEndpoint, {url: downloadUrl, storeId: activeStore, crumb});
            } catch (error) {
                toast.error(<Toast title={"Error"} content={error.message}/>);
            }
        }
    }

    return (
        <Container>

            <Dropzone autoUpload={true} getUploadParams={getUploadParams} maxSizeBytes={1342177000}
                      onChangeStatus={onDropzoneStatusChange}
                      styles={{dropzone: {overflow: "hidden", marginTop: '2rem', marginBottom: '2rem'}}}/>
            <FormGroup>
                { /* @ts-ignore */}
                <Select name="store" value={activeStore === -1 ? '' : activeStore} onChange={(e) => setActiveStore(e.target.value)}>
                    {stores.map((store, key) => (
                        <MenuItem key={key} value={store.id}>{store.name}</MenuItem>
                    ))}
                </Select>
            </FormGroup>
            <TextField fullWidth={true} type="url" placeholder="Import File From URL"
                       className="mt-3 mb-3" onKeyPress={demandDownload} value={downloadUrl}
                       onChange={(e) => setDownloadUrl(e.target.value)}/>
            <Row>
                {
                    categories.map((category, key) => (
                        <Col key={key}>
                            <Button color={"default"} variant={"contained"}
                                    id={`${category.title.toLowerCase()}-tab`}
                                    onClick={() => openCategory(category)}>{_.capitalize(category.title)}</Button>
                        </Col>
                    ))
                }
            </Row>
            <PulseLoader loading={isFetching}/>
            {(!isFetching) ?
                categories.map((category, categoryKey) => (
                    <Container key={categoryKey}
                               id={category.title}
                               className={(activeCategory === category.id) ? 'visible' : 'd-none'}>
                        {
                            stores.map((store, storeKey) => (
                                <StoreView key={storeKey} category={category} store={store}
                                           files={library.filter((file) => file.storeId === store.id && file.categoryId === category.id)}/>
                            ))
                        }
                    </Container>
                ))
                : null
            }
        </Container>
    );
}

export default connector(GalleryPage);
