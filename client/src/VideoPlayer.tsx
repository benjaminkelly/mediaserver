import React from 'react';
import Player from 'griffith';

interface IProps {
	src: string;
	title: string;
}

export default class VideoPlayer extends React.Component {

	props: IProps;
	videoNode: HTMLVideoElement | String | null;

	constructor(props: IProps) {
		super(props);
		this.props = props;
	}


	componentDidUpdate() {
		/*if (this.player && this.props.sources) {
			this.player.src(this.props.sources);
		}*/
	}
/*
	// destroy player on unmount
	componentWillUnmount() {
		if (this.player != null) {
			this.player.dispose();
		}
	}
 */
	render() {
		return (
			<div onClick={(e) => e.stopPropagation()}>
				{ /* @ts-ignore */ }
					<Player id={"player"} title={this.props.title} cover={this.props.src + "/thumb"} sources={{hd: {play_url: this.props.src + "/raw"}}}/>
					{ /*<video ref={ node => this.videoNode = node } id={'video'} className="video-js"></video> */ }
			</div>
		)
	}
}
