import React from 'react';
import { Route, RouteProps, Routes } from 'react-router-dom';
import Layout from './layout';
import LoginPage from './pages/login';
import RealmPage from './pages/realms';
import GalleryPage from './pages/gallery';
import SettingsPage from './pages/settings';

declare interface IProps extends RouteProps {
	children: JSX.Element;
	layoutProps: {
		title: string;
		hideHeader?: boolean;
		allowSocket?: boolean;
	}
}

const AppRoute = (props: IProps) => (
	<Route path={props.path!} element={
		<Layout { ...props.layoutProps } >
			{props.children}
		</Layout>
	}/>
);


export default function AppRoutes() {
	return (
		<div className="text-center">
			<Routes>
				<AppRoute path="/login" layoutProps={{ title: 'Index', hideHeader: true }}>
					<LoginPage/>
				</AppRoute>
				<AppRoute path="/realms" layoutProps={{ title: 'Realms' }}>
					<RealmPage/>
				</AppRoute>
				<AppRoute path="/gallery" layoutProps={{ title: 'Gallery', allowSocket: true }}>
					<GalleryPage/>
				</AppRoute>
				<AppRoute path="/settings" layoutProps={{ title: 'Settings' }}>
					<SettingsPage/>
				</AppRoute>
			</Routes>
		</div>
	)
}
