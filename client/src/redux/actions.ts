import axios from "axios";
import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import {GalleryFile} from "../models/GalleryFile";
import {Category} from "../models/Category";
import {Store} from "../models/Store";
import {CategoriesEndpoint, FilesEndpoint, StoresEndpoint} from "../Endpoints";

export type ModifyProps = { fileId: string };
export interface RenameProps extends ModifyProps {
	name: string;
}
export interface MoveProps extends ModifyProps {
	storeId: number
}

export const fetchCategories = createAsyncThunk("remote/FETCH_CATEGORIES", async () => {
	const response = await axios.get<Category[]>(CategoriesEndpoint);
	if (response.data) {
		return response.data;
	}
	return [];
});

export const fetchStores = createAsyncThunk("remote/FETCH_STORES", async () => {
	const response = await axios.get<Store[]>(StoresEndpoint);
	if (response.data) {
		return response.data;
	}
	return [];
})

export const fetchLibrary = createAsyncThunk("remote/FETCH_LIBRARY", async () => {
	const response = await axios.get<GalleryFile[]>(FilesEndpoint);
	if (response.data) {
		return response.data;
	}
	return [];
});

export const addFile = createAction<GalleryFile>("ADD_FILE");

export const removeFile = createAction<string>("REMOVE_FILE");

export const renameFile = createAsyncThunk("remote/RENAME_FILE", async (options: RenameProps) => {
	const response = await axios.patch(`${FilesEndpoint}/${options.fileId}`, { name: options.name });
	if (response.status === 200) {
		return options;
	}
	return null;
});

export const moveFile = createAsyncThunk("remote/MOVE_FILE", async (options: MoveProps) => {
	const response = await axios.patch(`${FilesEndpoint}/${options.fileId}`, { storeId: options.storeId });
	if (response.status === 200) {
		return options;
	}
	return null;
});

export const deleteFile = createAsyncThunk("remote/DELETE_FILE", async (galleryFile: GalleryFile) => {
	const response = await axios.delete(`${FilesEndpoint}/${galleryFile.id}`);
	if (response.status === 200) {
		return galleryFile;
	}
	return null;
});
