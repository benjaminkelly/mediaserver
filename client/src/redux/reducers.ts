import { fetchLibrary, addFile, deleteFile, fetchStores, fetchCategories, removeFile, renameFile, moveFile } from "./actions";
import _ from 'lodash';
import { createReducer } from '@reduxjs/toolkit';
import { Store } from "../models/Store";
import { Category } from "../models/Category";
import { GalleryFile } from "../models/GalleryFile";

export interface IReduxStore {
    socketID: string,
    crumb: string,
    library: GalleryFile[],
    stores: Store[],
    categories: Category[]
}

const initialState: IReduxStore = {
    socketID: '',
    crumb: '',
    library: [],
    stores: [],
    categories: []
}

export const reducer = createReducer(initialState, (builder) =>
    builder.addCase(fetchStores.fulfilled, (state, action) => {
        state.stores = action.payload;
    })
        .addCase(fetchCategories.fulfilled, (state, action) => {
            state.categories = action.payload;
        })
        .addCase(fetchLibrary.fulfilled, (state, action) => {
            state.library = action.payload;
        })
        .addCase(addFile, (state, action) => {
            let payload = action.payload;
            if (state.library.find((file) => file.id === payload.id) === undefined) {
                state.library.push(payload);
            }
        })
        .addCase(removeFile, (state, action) => {
            let fileId = action.payload;
            _.remove(state.library, (file) => file.id === fileId);
        })
        .addCase(renameFile.fulfilled, (state, action) => {
            if (action.payload != null) {
                let payload = action.payload;
                let fileIndex = state.library.findIndex((file) => file.id === payload.fileId);
                if (fileIndex >= 0) {
                    state.library[fileIndex].name = payload.name;
                }
            }
        })
        .addCase(moveFile.fulfilled, (state, action) => {
            if (action.payload != null) {
                let payload = action.payload;
                let fileIndex = state.library.findIndex((file) => file.id === payload.fileId);
                if (fileIndex >= 0) {
                    state.library[fileIndex].storeId = payload.storeId;
                }
            }
        })
        .addCase(deleteFile.fulfilled, (state, action) => {
            if (action.payload != null) {
                let payload = action.payload;
                _.remove(state.library, (file) => file.id === payload.id);
            }
        })
);
