import { useEffect, Fragment } from 'react';
import {io} from "socket.io-client";
import "vanillatoasts/vanillatoasts.css";
import VanillaToasts from "vanillatoasts";
import { connect, ConnectedProps } from "react-redux";
import { fetchLibrary, addFile, removeFile } from "../redux/actions";
import { toast } from 'react-toastify';
import Toast from './Card/Toast';
import { isGalleryFile } from '../models/GalleryFile';
import { FilesEndpoint } from '../Endpoints';

const mapDispatchToProps = {
	fetchLibrary,
	addFile,
	removeFile
}

const connector = connect(null, mapDispatchToProps);
type ReduxProps = ConnectedProps<typeof connector>;

function SocketHandler(props: ReduxProps) {

	const { addFile: addFileProp, removeFile: removeFileProp } = props;

	useEffect(() => {
		const socket = io({ secure: window.location.protocol.startsWith('https'), host: window.location.host, port: '8080', withCredentials: true, upgrade: true });
		socket.on("message", (message) => {
			console.log("Message", message);
		});
		socket.onAny((event) => {
			console.log("Event", event);
		});
		socket.on("spawnToast", (params) => {
			if (params && params.title && params.text && params.type) {
				VanillaToasts.create(params);
			}
		});
		socket.on("progress", (stats) => {
			if (stats.id && stats.percent && stats.name) {
				if (!toast.isActive(stats.id)) {
					toast.info(<Toast title={'Processing'} content={stats.name} />, {
						progress: stats.percent / 100,
						toastId: stats.id
					});
				}
				else {
					toast.update(stats.id, {
						progress: stats.percent / 100
					});
				}
			}
		});
		socket.on("addFile", (file) => {
			if (isGalleryFile(file)) {
				addFileProp(file);
				toast.info("", {
					icon: () => <img alt={file.name} src={`${FilesEndpoint}/${file.id}/thumb`}/>
				});
				VanillaToasts.create({
					title: "New File",
                	text: file.name + " has been uploaded.",
                	type: "success",
                	icon: `${FilesEndpoint}/${file.id}/thumb`
				});
			}
		});
		socket.on("removeFile", (fileId) => {
			if (typeof fileId === "string") {
				removeFileProp(fileId);
			}
		});
		// @ts-ignore
		window.socket = socket;
	}, [addFileProp, removeFileProp]);
	return (
		<Fragment/>
		);
}

export default connector(SocketHandler);
