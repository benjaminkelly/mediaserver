import React from 'react';

export default class Footer extends React.Component {
    
    render() {
        return (
            <footer>
            	<div id="patienceOverlay">
	                <div id="text">The server is undergoing maintenance.</div>
	            </div>
	       </footer>
            )
    }
    
}