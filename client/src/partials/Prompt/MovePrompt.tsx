import { Button, Dialog, DialogTitle, DialogActions, DialogContent, InputLabel, Select, MenuItem } from '@material-ui/core';
import { useState } from 'react';
import { Store } from '../../models/Store';

declare interface IProps {
    currentStore: number;
    stores: Store[];
    open: boolean;
    onMove: (storeId: number) => Promise<void>;
    onClose: () => void;
}

export default function MovePrompt(props: IProps) {

    const [store, setStore] = useState<number>(props.currentStore);

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle>Where should the file be moved to?</DialogTitle>
            <DialogContent>
                <InputLabel id={"move-selector-label"} />
                {/* @ts-ignore */}
                <Select value={store === -1 ? '' : store} labelId={"store-selector-label"} onChange={(e) => setStore(e.target.value)}>
                    {
                        props.stores.map((store, storeKey) => (
                            <MenuItem key={storeKey} value={store.id} >{store.name}</MenuItem>
                        ))
                    }
                </Select>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.onClose()}>Cancel</Button>
                <Button onClick={() => props.onMove(store)}>Accept</Button>
            </DialogActions>
        </Dialog>
    )
}