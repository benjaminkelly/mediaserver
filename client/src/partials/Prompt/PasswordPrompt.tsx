import { Button, Dialog, DialogTitle, DialogActions, DialogContent, TextField } from '@material-ui/core';
import { useState } from 'react';

declare interface IProps {
    open: boolean;
    onceDone: (password: string) => Promise<void>;
    onClose: () => void;
}

export default function PasswordPrompt(props: IProps) {

    const [password, setPassword] = useState("");

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle>What password would you prefer?</DialogTitle>
            <DialogContent>
                <TextField id="password" label="Password" type={"password"} value={password} onChange={(e) => setPassword(e.target.value)} />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.onClose()}>Cancel</Button>
                <Button onClick={() => props.onceDone(password)}>Accept</Button>
            </DialogActions>
        </Dialog>
    )
}