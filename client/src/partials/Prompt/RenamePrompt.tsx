import { Button, Dialog, DialogTitle, DialogActions, DialogContent, Input } from '@material-ui/core';
import { useState } from 'react';

declare interface IProps {
    placeholder: string;
    open: boolean;
    onRename: (name: string) => Promise<void>;
    onClose: () => void;
}

export default function RenamePrompt(props: IProps) {

    const [name, setName] = useState(props.placeholder);

    return (
        <Dialog open={props.open} onClose={ props.onClose }>
            <DialogTitle>What should the file be called?</DialogTitle>
            <DialogContent>
                <Input type="text" value={name} onChange={(e) => setName(e.target.value)} fullWidth={true} />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => props.onClose()}>Cancel</Button>
                <Button onClick={() => props.onRename(name)}>Accept</Button>
            </DialogActions>
        </Dialog>
    )
}