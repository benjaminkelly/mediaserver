import { trackWindowScroll } from 'react-lazy-load-image-component';
import { Box, Card, Grid } from "@material-ui/core";
import { Store } from "../../models/Store";
import { Category } from "../../models/Category";
import SimpleReactLightbox, { SRLWrapper } from 'simple-react-lightbox';
import ImageCard from "../Card/ImageCard";
import ContextMenu from "../contextMenu";
import VideoGallery from "../VideoGallery";
import { GalleryFile } from "../../models/GalleryFile";

declare interface IProps {
    category: Category,
    files: GalleryFile[],
    store: Store
}

function StoreView(props: IProps) {

    const { category, files, store } = props;

    return (
        <Grid container spacing={4}>
            <Grid className={"font-weight-bold"} item xs={12}>{store.name}</Grid>
            {
                (category.title.toLowerCase() === "videos") ?
                    <Grid container spacing={5} alignItems={"center"} justifyContent={"space-around"}>
                        <VideoGallery files={files} title={store.name} category={category} />
                    </Grid>
                    :
                    <SimpleReactLightbox>
                        <SRLWrapper>
                            <Grid container alignItems={"center"} justify={"space-around"}>
                                {files.map((file, key) => (
                                    <Box mt={5} key={key}>
                                        <Card style={{ width: '300px', height: '300px' }} elevation={4}>
                                            <ContextMenu file={file} store={store.name} category={category.title} >
                                                <ImageCard file={file} />
                                            </ContextMenu>
                                        </Card>
                                    </Box>
                                )
                                )}
                            </Grid>
                        </SRLWrapper>
                    </SimpleReactLightbox>
            }
        </Grid>
    );
}

export default trackWindowScroll(StoreView);
