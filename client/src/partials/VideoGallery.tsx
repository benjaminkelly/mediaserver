import { Fragment, useState } from 'react';
import { FaChevronCircleLeft, FaChevronCircleRight } from 'react-icons/all';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { GalleryFile } from "../models/GalleryFile";
import ContextMenu from "./contextMenu";
import { Figure } from "react-bootstrap";
import { Button, Card, Grid } from "@material-ui/core";
import VideoPlayerV2 from "../VideoPlayerV2";
import { Category } from "../models/Category";
import { FilesEndpoint } from '../Endpoints';

interface IProps {
    files: GalleryFile[],
    title: string,
    category: Category
}

export default function VideoGallery(props: IProps) {

    const { files, title, category } = props;

    const [lightboxDisplayed, setLightBoxDisplayed] = useState(false);
    const [fileId, setFileId] = useState(0);

    const handleClick = (key: number) => {
        setLightBoxDisplayed(true);
        setFileId(key);
    }

    const showPrevious = (e) => {
        e.stopPropagation();
        if (fileId - 1 < 0) {
            setFileId(files.length - 1);
        }
        else {
            setFileId(fileId - 1);
        }
    }

    const showNext = (e) => {
        e.stopPropagation();
        if (files.length <= fileId + 1) {
            setFileId(0);
        } else {
            setFileId(fileId + 1);
        }
    }

    const hideLightBox = () => {
        setLightBoxDisplayed(false);
    }

    return (
        <Fragment>
            {
                files.map((file, key) => (
                    <Grid item key={key} className={category.container}>
                        <Card style={{ width: '300px', cursor: 'pointer' }}>
                            <ContextMenu file={file} store={title} category={category.title}>
                                <LazyLoadImage src={`${FilesEndpoint}/${file.id}/thumb`}
                                    onClick={() => handleClick(key)} />
                            </ContextMenu>
                            <Figure.Caption>
                                {file.name}
                            </Figure.Caption>
                        </Card>
                    </Grid>
                ))
            }
            {
                lightboxDisplayed ?
                    <Card id="lightbox" onClick={hideLightBox}>
                        <Button onClick={showPrevious}><FaChevronCircleLeft/></Button>
                        <div onClick={(e) => e.stopPropagation()}>
                            <VideoPlayerV2 src={{ value: `${FilesEndpoint}/${files[fileId].id}`, content: files[fileId].name }} />
                        </div>
                        <Button onClick={showNext}><FaChevronCircleRight/></Button>
                    </Card>
                    : ""
            }
        </Fragment>
    );
}
