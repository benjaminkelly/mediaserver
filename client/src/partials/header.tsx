import { useNavigate } from 'react-router-dom';
import { Button, Navbar, Nav } from "react-bootstrap";
import { LeaveEndpoint } from "../Endpoints";


function Header() {
	const navigate = useNavigate();
	const leave = () => {
		fetch(LeaveEndpoint, { method: "POST" }).then(() => {
			navigate("/login", { replace: true });
		});
	}
	return (
		<Navbar bg={"light"} expand={"lg"} className={"justify-content-between"} sticky="top">
			<Navbar.Brand href="/gallery">Gallery</Navbar.Brand>
			<Navbar.Toggle aria-controls={"basic-navbar-nav"} />

			<Navbar.Collapse id={"basic-navbar-nav"} className={"justify-content-end"}>
				<Nav>
					<Nav.Link href={"/settings"}>Settings</Nav.Link>
					<Nav.Item>
						<Button variant="outline-primary" onClick={leave}>Leave</Button>
					</Nav.Item>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}

export default Header;
