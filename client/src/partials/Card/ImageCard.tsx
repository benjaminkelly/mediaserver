import { LazyLoadImage } from 'react-lazy-load-image-component';
import _ from "lodash";
import { GalleryFile } from '../../models/GalleryFile';
import { FilesEndpoint } from "../../Endpoints";

declare interface IProps {
    file: GalleryFile
}

export default function ImageCard(props: IProps) {

    const { file } = props;

    return (
        <a href={`${FilesEndpoint}/${file.id}/raw`}>
            <LazyLoadImage src={`${FilesEndpoint}/${file.id}/thumb`} alt={_.capitalize(file.name)} />
        </a>
    )
}
