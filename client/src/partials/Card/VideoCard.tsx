import React from 'react';
import {LazyLoadImage} from 'react-lazy-load-image-component';
import _ from "lodash";
import {GridListTileBar} from "@material-ui/core";
import {GalleryFile} from '../../models/GalleryFile';
import {FilesEndpoint} from "../../Endpoints";

declare interface IProps {
    file: GalleryFile,
    store: string
}

export default function VideoCard(props: IProps) {

    const {file} = props;

    return (
        <React.Fragment>
            <LazyLoadImage src={`${FilesEndpoint}/${file.id}/thumb`} alt={file.name}/>
            { /*@ts-ignore*/}
            <video srl_video_loop={"true"} srl_video_caption={file.name}
                   srl_video_thumbnail={`${FilesEndpoint}/${file.id}/preview`}
                   width={'100%'}
                   height={'100%'}
                   srl_video_controls={'true'}
                   srl_video_autoplay={'true'}
                   style={{ display: 'none' }}
                   src={`${FilesEndpoint}/${file.id}/raw`}>
            </video>
            <GridListTileBar title={_.capitalize(file.name)}/>
        </React.Fragment>
    )
}
