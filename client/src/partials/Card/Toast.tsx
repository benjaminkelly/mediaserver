declare interface IProps {
    title: string;
    content: string;
    icon?: string;
}

export default function Toast(props: IProps) {
    return (
        <div className={"toast-container"}>
            {
                (props.icon) ?
                    <img className={"toast-icon"} src={props.icon} alt="Toast Icon"></img>
                : null
            }
            <h1 className={"toast-title"}>{props.title}</h1>
            <p className={"toast-message"}>{props.content}</p>
        </div>
    )
}