import { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { GalleryFile } from '../models/GalleryFile';
import 'react-contexify/dist/ReactContexify.min.css';
import RenamePrompt from '../partials/Prompt/RenamePrompt';
import MovePrompt from '../partials/Prompt/MovePrompt';
import { Item, Menu, useContextMenu, animation, theme } from 'react-contexify';
import { FaEllipsisV, RiDeleteBin6Line, RiFileDownloadFill, RiSendPlaneFill } from "react-icons/all";
import { deleteFile, renameFile, moveFile } from "../redux/actions";
import { FilesEndpoint } from "../Endpoints";

declare interface IProps extends ReduxProps {
    file: GalleryFile,
    children: JSX.Element,
    store: string,
    category: string
}

const mapStateToProps = (store) => {
    return {
        stores: store.stores
    }
}

const mapDispatchToProps = {
    deleteFile,
    renameFile,
    moveFile
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type ReduxProps = ConnectedProps<typeof connector>;

function ContextMenu(props: IProps) {

    const { file, children } = props;

    const [renamePromptOpen, setRenamePromptOpen] = useState(false);
    const [movePromptOpen, setMovePromptOpen] = useState(false);

    const MenuId = file.id;

    const { show } = useContextMenu({
        id: MenuId
    });

    const deleteFile = async () => {
        if (window.confirm("Are your sure you want to delete: " + file.name)) {
            await props.deleteFile(file);
        }
    }
    const renameFile = async (preferredName: string) => {
        setRenamePromptOpen(false);
        if (preferredName != null && preferredName.trim() !== "" && preferredName !== file.name) {
            await props.renameFile({ fileId: file.id, name: preferredName });
        }
    }
    const moveFile = async (preferredStoreId: number) => {
        setMovePromptOpen(false);
        if (preferredStoreId >= 0 && preferredStoreId !== file.storeId) {
            await props.moveFile({ fileId: file.id, storeId: preferredStoreId });
        }
    }
    const downloadFile = async () => {
        let link = document.createElement("a");
        link.href = `${FilesEndpoint}/${file.id}/raw/attachment`;
        link.rel = "noreferrer"
        link.target = "_blank";
        link.click();
    }

    return (
        <div onContextMenu={show}>
            <RenamePrompt placeholder={file.name} open={renamePromptOpen} onRename={async (name) => await renameFile(name)} onClose={() => setRenamePromptOpen(false)} />
            <MovePrompt stores={props.stores} currentStore={file.storeId} open={movePromptOpen} onMove={async (storeId) => await moveFile(storeId)} onClose={() => setMovePromptOpen(false)} />
            <Menu id={MenuId} theme={theme.dark} animation={animation.slide}>
                <Item onClick={downloadFile}>
                    <RiFileDownloadFill />
                    <span>Download</span>
                </Item>
                <Item onClick={() => setRenamePromptOpen(true)}>
                    <FaEllipsisV />
                    <span>Rename</span>
                </Item>
                <Item onClick={() => setMovePromptOpen(true)}>
                    <RiSendPlaneFill />
                    <span>Move</span>
                </Item>
                <Item onClick={deleteFile}>
                    <RiDeleteBin6Line />
                    <span>Delete</span>
                </Item>
            </Menu>
            {children}
        </div>
    )
}

export default connector(ContextMenu);
