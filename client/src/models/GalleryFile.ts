export type GalleryFile = {
    id: string,
    categoryId: number,
    storeId: number,
    name: string,
    extension: string
}

export function isGalleryFile(input: any): input is GalleryFile {
    return (typeof input === 'object' && 
    input["id"] != null && 
    input["name"] != null && 
    input["storeId"] != null);
}