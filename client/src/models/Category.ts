export type Category = {
    id: number,
    title: string,
    container?: string
}
