export type Answer<T> = {
    code: string,
    comment: string,
    data: T
}
