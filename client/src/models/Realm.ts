export type Realm = {
    id: number,
    name: string,
    description: string,
    protected: boolean
}
