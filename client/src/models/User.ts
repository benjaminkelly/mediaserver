export type User = {
    id: number;
    name: string;
    realmId: number;
    verified: boolean;
}