import * as React from 'react';
import {Component} from 'react';
import VidPlayIcon from '@atlaskit/icon/glyph/vid-play';
import VidPauseIcon from '@atlaskit/icon/glyph/vid-pause';
import VidFullScreenOnIcon from '@atlaskit/icon/glyph/vid-full-screen-on';
import VolumeIcon from '@atlaskit/icon/glyph/hipchat/outgoing-sound';
import Button from '@atlaskit/button';
import {ItemType} from '@atlaskit/single-select';
import ReactSelect from "react-select";
import Spinner from '@atlaskit/spinner';
import Video, {SetPlaybackSpeed, VideoTextTracks} from 'react-video-renderer';
import {
    AppWrapper,
    VideoRendererWrapper,
    ErrorWrapper,
    VideoWrapper,
    MutedIndicator,
    LeftControls,
    RightControls,
    ControlsWrapper,
    TimeRangeWrapper,
    CurrentTime,
    TimebarWrapper,
    VolumeWrapper,
    SpinnerWrapper,
    PlaybackSpeedWrapper,
    SubtitlesWrapper
} from './styled';
import {TimeRange} from './timeRange';
import makeAnimated from "react-select/animated";

export interface ContentSource {
    content: string;
    value: string;
    textTracks?: VideoTextTracks;
}

export interface AppState {
    currentSource: ContentSource;
    playbackSpeed: number;
    isVisible: Boolean;
}

type PlaybackSpeedSource = ItemType & {
    value: number;
};

type PlaybackSpeedSourceSelect = Array<{ heading?: string, items: PlaybackSpeedSource[] }>;

const playbackSpeeds: PlaybackSpeedSourceSelect = [
    {
        items: [
            {value: 0.5, label: '0.5'},
            {value: 0.75, label: '0.75'},
            {value: 1, label: 'normal'},
            {value: 1.25, label: '1.25'},
            {value: 1.5, label: '1.5'},
            {value: 1.75, label: '1.75'},
            {value: 2, label: '2'},
        ],
    },
];

const selectDefaultPlaybackSpeed = (playbackSpeed: number): PlaybackSpeedSource => {
    let source = playbackSpeeds[0].items.find((item) => item.value === playbackSpeed);
    if (!source) {
        source = {value: 1}
    }
    return source;
}

interface IProps {
    src: ContentSource,
    children?: React.ReactNode
}

export default class App extends Component<IProps, AppState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            currentSource: this.props.src,
            playbackSpeed: 1,
            isVisible: true
        };
    }

    componentWillUnmount() {
        this.setState({
            playbackSpeed: 0,
            isVisible: false
        })
    }

    subtitlesTrackRef: React.RefObject<HTMLTrackElement> | undefined;

    onNavigate = (navigate: Function) => (value: number) => {
        navigate(value);
    };

    onVolumeChange = (setVolume: Function) => (e: any) => {
        const value = e.target.value;
        setVolume(value);
    };

    renderSpinner = () => {
        return (
            <SpinnerWrapper>
                <Spinner size="xlarge"/>
            </SpinnerWrapper>
        );
    };

    private secondsToTimestamp = (seconds: number) => (Math.floor(seconds / 60)).toString() + ":" + Math.round(seconds) % 60

    private changePlaybackSpeed = (setPlaybackSpeed: SetPlaybackSpeed) => (playbackSpeed: number) => {
        setPlaybackSpeed(playbackSpeed);
        this.setState({playbackSpeed});
    };

    private getDefaultTimeLocalStorageKey() {
        const {currentSource} = this.state;
        return `react-video-render-default-time-${currentSource.value}`;
    }

    private get defaultTime(): number {
        const savedTime = localStorage.getItem(this.getDefaultTimeLocalStorageKey());

        if (savedTime) {
            return JSON.parse(savedTime);
        } else {
            return 0;
        }
    }

    private onTimeChange = (currentTime: number) => {
        localStorage.setItem(this.getDefaultTimeLocalStorageKey(), JSON.stringify(currentTime));
    };

    render() {
        const {src} = this.props;
        const {playbackSpeed} = this.state;

        return (
            <AppWrapper>
                <VideoRendererWrapper>
                    <Video
                        sourceType={"video"}
                        crossOrigin={"anonymous"}
                        src={`${src.value}/raw`}
                        autoPlay={false}
                        textTracks={src.textTracks}
                        poster={`${src.value}/thumb`}
                        defaultTime={this.defaultTime}
                        onTimeChange={this.onTimeChange}
                    >
                        {(video, videoState, actions) => {
                            const {status, currentTime, buffered, duration, volume, isLoading} = videoState;
                            if (status === 'errored') {
                                return <ErrorWrapper>Error</ErrorWrapper>;
                            }
                            const button =
                                status === 'playing' ? (
                                    <Button iconBefore={<VidPauseIcon label="play"/>} onClick={actions.pause}/>
                                ) : (
                                    <Button iconBefore={<VidPlayIcon label="pause"/>} onClick={actions.play}/>
                                );
                            const fullScreenButton = (
                                <Button iconBefore={<VidFullScreenOnIcon label="fullscreen"/>}
                                        onClick={actions.requestFullscreen}/>
                            );

                            const getPlaybackSpeedLabel = (item: PlaybackSpeedSource): string => {
                                return item.toString();
                            }

                            const getPlaybackSpeedValue = (item: PlaybackSpeedSource): string => {
                                return item.value.toString();
                            }

                            const playbackSpeedSelect = (
                                <PlaybackSpeedWrapper>

                                    <ReactSelect name={"Speed"} options={playbackSpeeds[0].items}
                                                 isSearchable={false}
                                                 value={selectDefaultPlaybackSpeed(playbackSpeed)}
                                                 components={makeAnimated()}
                                                 isClearable={false}
                                                 menuPlacement={"top"}
                                                 getOptionLabel={getPlaybackSpeedLabel}
                                                 getOptionValue={getPlaybackSpeedValue}
                                                 onChange={(item: PlaybackSpeedSource) => {
                                                     if (item) {
                                                         this.changePlaybackSpeed(actions.setPlaybackSpeed)(parseFloat(item.value))
                                                     }
                                                 }}
                                                 menuPosition={"fixed"}/>
                                </PlaybackSpeedWrapper>
                            );

                            const currentEnglishSubtitlesCues = videoState.currentActiveCues('subtitles', 'en');
                            const subtitles =
                                currentEnglishSubtitlesCues && currentEnglishSubtitlesCues.length > 0 ? (
                                    <SubtitlesWrapper>
                                        {Array.prototype.map.call(currentEnglishSubtitlesCues, (cue: any, index: number) => (
                                            <span key={index}>{cue.text}</span>
                                        ))}
                                    </SubtitlesWrapper>
                                ) : undefined;

                            return (
                                <VideoWrapper>
                                    {isLoading && this.renderSpinner()}
                                    {this.state.isVisible && video}
                                    <TimebarWrapper>
                                        <TimeRangeWrapper>
                                            <TimeRange
                                                currentTime={currentTime}
                                                bufferedTime={buffered}
                                                duration={duration}
                                                onChange={(time) => this.onNavigate(actions.navigate)(time)}
                                            />
                                        </TimeRangeWrapper>
                                        <ControlsWrapper>
                                            <LeftControls>
                                                {button}
                                                <CurrentTime>
                                                    {this.secondsToTimestamp(currentTime)} / {this.secondsToTimestamp(duration)}
                                                </CurrentTime>
                                                <VolumeWrapper>
                                                    <MutedIndicator isMuted={videoState.isMuted}/>
                                                    <Button onClick={actions.toggleMute}
                                                            iconBefore={<VolumeIcon label="volume"/>}/>
                                                    <input
                                                        type="range"
                                                        step={0.01}
                                                        value={volume}
                                                        max={1}
                                                        onChange={this.onVolumeChange(actions.setVolume)}
                                                    />
                                                </VolumeWrapper>
                                            </LeftControls>
                                            <RightControls>
                                                {playbackSpeedSelect}
                                                {fullScreenButton}
                                            </RightControls>
                                        </ControlsWrapper>
                                    </TimebarWrapper>
                                    {subtitles}
                                </VideoWrapper>
                            );
                        }}
                    </Video>
                </VideoRendererWrapper>
            </AppWrapper>
        );
    }
}
