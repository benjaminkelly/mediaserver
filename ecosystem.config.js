const path = require('path');

console.log(path.join(__dirname, '/server/'));

module.exports = {
  apps : [{
    name: 'API',
    script: 'server/app.js',
    cwd: path.join(__dirname, '/server/'),
    instance_var: 'INSTANCE_ID',
    autorestart: true,
    watch: ["server/Workers"],
    watch_delay: 1000,
    ignore_watch : ["node_modules", "client", "server"],
    watch_options: {
      "followSymlinks": false
    },
    max_memory_restart: '4G'
  },
  {
    name: 'Front-End',
    script: 'npm',
    args: 'run start',
    autorestart: true,
    max_memory_restart: '2G'
  }
]
};
