#!/bin/bash

echo "Updating apt"

sudo apt update 2>/dev/null

echo "Installing prerequisites"

sudo apt install ffmpeg redis handbrake-cli libgif-dev -y 2>/dev/null

echo "Preparing local npm packages"

yarn install