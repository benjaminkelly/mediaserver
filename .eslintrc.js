module.exports = {
    root: true,
    'extends': [
        'eslint:recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript'
        ],
    plugins: ['import'],
    "parser": "@typescript-eslint/parser",
    rules: {
        'comma-dangle': ['warn', 'only-multiline'],
        eqeqeq: ['warn', 'smart'],
        camelcase: 'warn',
        'no-undef': 'off',
        'import/no-cycle': 'warn',
        'no-empty': 'warn',
        'no-unused-vars': 'off',
        'no-mixed-spaces-and-tabs': 'off'
    }
};
