import "reflect-metadata";
import Hapi from '@hapi/hapi';
import fs from 'fs';
import path from 'path';
import AdminBro from "admin-bro";
import { Database, Resource } from '@admin-bro/typeorm';
import { validate } from 'class-validator';
Resource.validate = validate;
import AdminBroConsole from '@admin-bro/hapi';
import Delegator from "./Workers/Delegate";
import {mapModels, getAdminBroOptions} from "./DatabaseModel/Model";
import cleanUpMediaDir from "./Workers/Init";

AdminBro.registerAdapter({ Database, Resource });

mapModels().then(async () => {

    await cleanUpMediaDir();

    let httpsOptions: any = false;
    let config = null;
    try {
        config = require("../config/default");
        httpsOptions = {
            key: fs.readFileSync(path.join(__dirname, '..', 'Certificates', config.TLS.key)),
            cert: fs.readFileSync(path.join(__dirname, '..', 'Certificates', config.TLS.cert))
        }
    } catch (e) {
        console.info("No https certificates provided, see docs to find out more.");
    }

    const hapiServer = new Hapi.Server({
        port: 8000,
        host: "127.0.0.1",
        routes: {
            cors: {
                origin: ['*'],
                maxAge: 60,
            }
        },
        state: {
            strictHeader: true,
            ignoreErrors: true,
            isSecure: true,
            isSameSite: 'Strict'
        },
        tls: httpsOptions
    });

    await hapiServer.register({
        plugin: AdminBroConsole,
        options: getAdminBroOptions()
    });

    const delegator = new Delegator();
    await delegator.invoke();

    try {
        await hapiServer.start();
        console.log(`To access the applications database visit ${hapiServer.info.uri}/admin`);
    } catch (e) {
        console.error(`The server failed to start at port ${hapiServer.info.port}`, e);
    }
});
