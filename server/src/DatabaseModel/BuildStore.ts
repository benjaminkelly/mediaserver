import path from 'path';
import glob from 'fast-glob';
import fse from 'fs-extra';
import slash from "../utils/slash";
import {PUBLICDIR} from "../Workers/Order/Constants";
import Store from "./models/store";
import {Connection} from "typeorm";

export default async function buildStore(connection: Connection) {
    let storeRepo = connection.getRepository(Store);
    let stores = await storeRepo.find({select: ["name"]});
    let dbStores = stores.map((store) => store.name);

    let mediaPath = slash(path.join(PUBLICDIR, 'media'));
    await fse.ensureDir(mediaPath);
    let localStoresStream = glob.stream(mediaPath + '/*', {onlyDirectories: true});

    for await (const localStorePath of localStoresStream) {
        const localStore = path.basename(localStorePath.toString());
        if (dbStores.includes(localStore) === false) {
            await storeRepo.insert({
                name: localStore
            });
        }
    }
}
