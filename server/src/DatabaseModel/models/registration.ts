import {
	BeforeInsert,
	Column,
	Entity,
	Generated,
	PrimaryGeneratedColumn, getRepository, OneToMany, BaseEntity, RelationId
} from "typeorm";
import User from "./user";

function tomorrow() {
	return Date.now() + 86400000;
}

@Entity()
export default class Registration extends BaseEntity {

	@PrimaryGeneratedColumn("increment")
	id: number;

	@Generated("uuid")
	@Column({ type: "uuid", unique: true, nullable: false })
	uuid: string;

	@Column({ default: tomorrow(), nullable: false })
	validUntil: Date;

	@OneToMany(() => User, (user) => user.id)
	user: User;

	@RelationId((registration: Registration) => registration.user)
	userId: number;

	@BeforeInsert()
	async clearAllExpiredRegistrations() {
		let registrationRepo = getRepository(Registration);
		let registrations = await registrationRepo.find();

		for await (const registration of registrations) {
			if (registration.validUntil < new Date()) {
				let userID = registration.user.id;
				await registrationRepo.delete({ id: userID });
			}
		}
	}
}
