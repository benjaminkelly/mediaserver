import {
    BaseEntity, Column,
    Entity, Index,
    JoinColumn,
    ManyToOne,
    RelationId
} from 'typeorm';
import Realm from "./realm";
import Store from "./store";

@Entity("realmHasStore")
@Index(["realm", "store"], { unique: true })
export default class RealmHasStore extends BaseEntity {

    @Column({ generated: 'uuid', nullable: true, primary: true })
    id: string;

    @RelationId((realmHasStore: RealmHasStore) => realmHasStore.realm)
    realmId: number;

    @RelationId((realmHasStore: RealmHasStore) => realmHasStore.store)
    storeId: number;

    @ManyToOne(() => Realm, (realm) => realm.id)
    @JoinColumn({ name: "realmId" })
    realm: Realm;

    @ManyToOne(() => Store, (store) => store.id)
    @JoinColumn({ name: "storeId" })
    store: Store;
}
