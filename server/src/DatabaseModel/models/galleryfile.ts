import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId} from "typeorm";
import Store from "./store";

@Entity({ name: "galleryfile" })
export default class GalleryFile extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;
    @Column({nullable: false})
    name: string;
    @RelationId((galleryFile: GalleryFile) => galleryFile.store)
    storeId: number;
    @ManyToOne(() => Store, { eager: true })
    store: Store;
    @Column({ nullable: false })
    categoryId: number;
    @Column({ nullable: false })
    extension: string;

    // definitely not a part of db
    socketId: string;
    // by no mean a port of the db
    processId: string;
}
