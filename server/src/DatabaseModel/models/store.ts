import {Column, ManyToMany, Entity, PrimaryGeneratedColumn, BaseEntity} from "typeorm";
import Realm from "./realm";

@Entity()
export default class Store extends BaseEntity {

    @PrimaryGeneratedColumn("increment")
    id: number;

    @Column({ nullable: false })
    name: string;

    @ManyToMany(() => Realm, (realm) => realm.stores, { nullable: true })
    realms: Realm[];
}
