import {
    Column,
    BeforeInsert,
    OneToMany, Entity, ManyToMany, PrimaryGeneratedColumn, BaseEntity, JoinTable
} from 'typeorm';
import argon2 from 'argon2';
import User from "./user";
import Store from "./store";

@Entity()
export default class Realm extends BaseEntity {

    @PrimaryGeneratedColumn("increment")
    id: number;

    @Column({nullable: false})
    name: string;

    @Column({ nullable: true })
    description: string;

    @Column({default: false})
    hidden: boolean;

    @Column({default: ''})
    password: string;

    @OneToMany(() => User, (user) => user.realm)
    users: User[];

    @ManyToMany(() => Store, (store) => store.realms, { eager: true })
    @JoinTable({ name: "realmHasStore",
        joinColumn: {
        name: "realmId", referencedColumnName: "id"
        },
        inverseJoinColumn: {
        name: "storeId", referencedColumnName: "id"
        }
    })
    stores: Store[];

    @BeforeInsert()
    async hashPassword() {
        if (this.password == null) {
            this.password = "";
        }
        this.password = await argon2.hash(this.password);
    }

    async isAuthorised(passwordInput) {
        if (this.password === "") {
            return this.password === passwordInput;
        }
        return await argon2.verify(this.password, passwordInput);
    }
}
