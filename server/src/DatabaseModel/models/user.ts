import {
	BeforeInsert,
	ManyToOne,
	Column,
	OneToMany,
	Entity, PrimaryGeneratedColumn, RelationId, BaseEntity, CreateDateColumn
} from "typeorm";
import {IsBoolean, IsEmail, IsNotEmpty} from 'class-validator';
import Redis from 'ioredis';
import JSONCache from 'redis-json';
import argon2 from 'argon2';
import Realm from "./realm";
import Token from "./token";
import Registration from "./registration";

const redis = new Redis();
const jsonCache = new JSONCache<UserValues>(redis, {prefix: 'cache:'});

export type UserValues = {
	id?: number,
	name?: string,
	storeIds?: number[],
	realmId?: number;
	verified?: boolean,
	error?: string
}

function getUserValues(user: User): UserValues {
	let storeIds = [];
	if (user.realm != null && user.realm.stores != null) {
		storeIds = user.realm.stores.map((store) => store.id);
	}
	return { id: user.id, error: null, name: user.name, realmId: user.realmId, storeIds, verified: user.verified };
}

@Entity()
export default class User extends BaseEntity {

	@PrimaryGeneratedColumn("increment")
	id: number;

	@IsNotEmpty()
	@Column({ unique: true, nullable: false })
	name: string;

	@Column({ default: '' })
	password: string;

	@IsEmail()
	@Column({ nullable: false })
	email: string;

	@CreateDateColumn()
	created: Date;

	@IsBoolean()
	@Column({ default: false })
	verified: boolean;

	@ManyToOne(() => Realm, {eager: true})
	realm: Realm;

	@RelationId((user: User) => user.realm)
	realmId: number;

	@OneToMany(() => Token, (token) => token.user)
	tokens: Token[];

	@OneToMany(() => Registration, (registration) => registration.user)
	registrations: Registration[];

	@BeforeInsert()
	async hashPassword() {
		if (this.password === null) {
			this.password = "";
		}
		this.password = await argon2.hash(this.password);
	}

	async isAuthorised(passwordInput) {
		if (this.password === "") {
			return this.password === passwordInput;
		}
		return await argon2.verify(this.password, passwordInput);
	}

	static async probeCache(token: string): Promise<UserValues> | null {
		return await jsonCache.get(token);
	}

	static async persistInCacheAndGetValues(token: string, user: User) {
		let userValues = getUserValues(user);
		await jsonCache.set(token, userValues, { expire: 10 });
		return userValues;
	}
}
