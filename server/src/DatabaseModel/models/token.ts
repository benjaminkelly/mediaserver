import {
	ManyToOne,
	Column,
	Generated, PrimaryGeneratedColumn, Entity, RelationId, BaseEntity, AfterLoad, getRepository
} from "typeorm";

import User from "./user";

function minuteDelay(minutes): Date {
	return new Date(Date.now() + 60000 * minutes);
}

@Entity()
export default class Token extends BaseEntity {

	@PrimaryGeneratedColumn("increment")
	id: number;

	@Generated("uuid")
	@Column({ type: "uuid", unique: true })
	value: string;

	@Column({ default: () => minuteDelay(30).valueOf(), nullable: false })
	validUntil: Date;

	@ManyToOne(() => User, { eager: true, nullable: false })
	user: User;

	@RelationId((token: Token) => token.user)
	userId: number;


	@AfterLoad()
	async removeExpiredToken() {
		if (this.validUntil.valueOf() <= new Date().valueOf()) {
			this.user = null;
			let tokenRepo = getRepository(Token);
			await tokenRepo.delete({ id: this.id });
		}
		else {
			// @ts-ignore
			this.validUntil = minuteDelay(30).valueOf();
		}
	}
}
