import Store from './models/store';
import buildStore from "./BuildStore";
import Realm from "./models/realm";
import User from "./models/user";
import Token from "./models/token";
import Registration from "./models/registration";
import {Connection, ConnectionOptions, createConnection} from "typeorm";
import passwordFeature from "@admin-bro/passwords";
import argon2 from "argon2";
import {AdminBroOptions} from "admin-bro";
import RealmHasStore from "./models/realmhasstore";
import GalleryFile from "./models/galleryfile";
import populateDb from "./PopulateDb";
import fse from 'fs-extra';
import path from "path";

export async function mapModels(): Promise<Connection> {

    let config: ConnectionOptions = {
        "type": "better-sqlite3",
        "database": path.join(__dirname, "..", "..", "database.sqlite"),
        "synchronize": (process.env.NODE_ENV !== 'production'),
        "entities": [path.join(__dirname, "models/**")],
        "cache": {
            type: "ioredis",
            tableName: "mediaserver-query-cache"
        }
    }

    fse.pathExists("../../ormconfig.custom.json").then((customConfigExists) => {
        if (customConfigExists) {
            config = require("../../ormconfig.custom.json");
        }
    });

    let connection: Connection;

    try {
        connection = await createConnection(config);
        console.log(`Using a ${connection.options.type} connection`);
    } catch (error) {
        console.error('Unable to connect to the database:\n', error);
        console.info("\n\nPlease read the installation instructions for further information.");
        process.exit();
    }

    await populateDb(connection);
    await buildStore(connection);

    return connection;
}

export function getAdminBroOptions(): AdminBroOptions {
    return {
        resources: [
            {
                resource: Realm,
                features: [passwordFeature({
                    properties: {
                        password: 'password',
                        encryptedPassword: 'password'
                    },
                    hash: async (password) => {
                        if (password === "") {
                            return password;
                        }
                        return await argon2.hash(password);
                    },
                })]
            },
            {resource: Store},
            {resource: RealmHasStore,
                options: {
                properties: {
                    realmId: {
                        name: 'realm',
                        label: 'realm',
                        isVisible: { edit: true, list: true },
                        isTitle: true
                    },
                    storeId: {
                        name: 'store',
                        label: 'store',
                        isVisible: { edit: true, list: true }
                    }
                }
                }
            },
            {
                resource: User,
                features: [passwordFeature({
                    properties: {
                        password: 'password',
                        encryptedPassword: 'password'
                    },
                    hash: async (password) => {
                        if (password === "") {
                            return password;
                        }
                        return await argon2.hash(password);
                    },
                })]
            },
            {resource: Token},
            {resource: Registration},
            {resource: GalleryFile}
        ]
    }
}

