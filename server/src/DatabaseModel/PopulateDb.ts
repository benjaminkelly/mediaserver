import {Connection} from "typeorm";
import User from "./models/user";

export default async function populateDb(connection: Connection) {
    let userRepo = connection.getRepository(User);
    let users = await userRepo.count();
    // register a guest and admin account
    if (users === 0) {
        await userRepo.save({
            name: 'Admin',
            password: '123',
            realmId: 1,
            email: 'example.example@domain.com',
            verified: true
        });
        await userRepo.save({
            name: 'Guest',
            password: '',
            email: 'example.example@domain.com',
            verified: true
        });
    }
}
