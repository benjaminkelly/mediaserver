const isStream = (stream) =>
	stream !== null &&
	typeof stream === 'object' &&
	typeof stream.pipe === 'function';

export default isStream;

export function isReadableStream(stream) {
    return isStream(stream) &&
    stream.readable !== false &&
    typeof stream._read === 'function' &&
    typeof stream._readableState === 'object';
}
    