import { Plugin, Server } from '@hapi/hapi';
import RouteLogic from '../routeLogic';

interface PluginOptions {
    routeLogic: RouteLogic;
}

const plugin: Plugin<PluginOptions> = {
    name: 'Store Routes',
    version: '1.0.0',
    register(server: Server, options: PluginOptions) {
        const { routeLogic } = options;
        server.route({
            method: 'GET',
            path: '/stores',
            handler: async (request) => await routeLogic.getStores(request),
            options: {
                description: 'Gets the users store names'
            }
        });
    }
}

export default plugin;