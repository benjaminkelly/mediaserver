import { Lifecycle, Plugin, Request, Server } from "@hapi/hapi";
import { Categories, FileNameEnum, MEDIAPATH } from "../../Order/Constants";
import SocketIO from 'socket.io';
import Uploader from "../../Transmission/Upload";
import Downloader from "../../Transmission/Download";
import { guessFileCategory } from "../../Order/Inspector";
import Boom from "@hapi/boom";
import Joi from "joi";
import DeletionManager from "../../FileManager/Delete";
import Gatherer, { FileResponseToolkit } from "../../FileManager/Gather";
import ModificationManager from "../../FileManager/Modify";

interface PluginOptions {
    io: SocketIO.Server;
}

const plugin: Plugin<PluginOptions> = {
    name: "File Routes",
    version: "1.0.0",
    register(server: Server, options: PluginOptions) {
        const gatherer: Gatherer = new Gatherer();
        const { io } = options;
        const uploadLogic = Uploader.getInstance(io);
        const downloadLogic = Downloader.getInstance(io);

        server.route({
            method: 'GET',
            path: '/library',
            handler: async (request) => gatherer.getLibrary(request),
            options: {
                description: 'Lists the complete library (constraints optional)'
            }
        });

        server.route({
            method: 'DELETE',
            path: '/library',
            handler: async (request) => await DeletionManager.deleteFile(request, io),
            options: {
                validate: {
                    payload: {
                        id: Joi.string().uuid({ version: "uuidv4" }).required()
                    }
                },
                description: 'Deletes a file'
            }
        });

        server.route({
            method: 'GET',
            path: "/media/{param*4}",
            handler: {
                directory: {
                    path: MEDIAPATH,
                    index: false,
                    redirectToSlash: true,
                    listing: false,
                }
            },
            options: {
                description: 'Hosts the library'
            }
        });
        server.route({
            method: 'GET',
            path: "/files",
            handler: async (request) => await gatherer.getFiles(request),
            options: {
                description: 'Lists all files the user is authorised to see (/library v2)',
                validate: {
                    query: {
                        store: Joi.number().min(0).optional(),
                        category: Joi.number().min(0).max(Categories.length - 1).optional()
                    },
                    options: {
                        allowUnknown: false
                    }
                }
            }
        });

        server.route({
            method: 'GET',
            path: "/files/{id}/{fileType}/{mode?}",
            handler: async (request, h: FileResponseToolkit) => await gatherer.getFileById(request, h),
            options: {
                validate: {
                    params: {
                        id: Joi.string().uuid({ version: "uuidv4" }).required(),
                        fileType: Joi.string().valid(...Object.values(FileNameEnum)).insensitive().required(),
                        mode: Joi.allow("attachment", "inline", false).default(false).optional()
                    },
                    options: {
                        allowUnknown: false
                    }
                },
                description: 'Returns a file with the given id'
            }
        });

        server.route({
            method: 'DELETE',
            path: "/files/{id}",
            handler: async (request) => await DeletionManager.deleteFile(request, io),
            options: {
                validate: {
                    params: {
                        id: Joi.string().uuid({ version: "uuidv4" }).required()
                    },
                    options: {
                        allowUnknown: false
                    }
                },
                description: 'Deletes a file with the given id'
            }
        });

        server.route({
            method: 'PATCH',
            path: "/files/{id}",
            handler: async (request) => await ModificationManager.modifyFile(request, io),
            options: {
                validate: {
                    payload: {
                        name: Joi.string().disallow('/', '\\').optional(),
                        storeId: Joi.number().min(0).optional()
                    },
                    params: {
                        id: Joi.string().uuid({ version: "uuidv4" }).required()
                    },
                    options: {
                        allowUnknown: false
                    }
                },
                description: 'Renames / Moves a file with the given id'
            }
        });

        function notifyUpload(request: Request): Lifecycle.ReturnValue {
            if (request.payload['file'] != null && request.payload['file'].hapi.filename != null) {
                let filename = request.payload['file'].hapi.filename;
                let category = guessFileCategory(filename);
                if (category == null) {
                    return Boom.unsupportedMediaType();
                }
                console.log("Upload in progress: ", filename);
                return Categories.indexOf(category);
            }
            throw new Error("Internal error");
        }

        server.route({
            method: 'POST',
            path: '/fileUpload',
            options: {
                pre: [
                    { method: notifyUpload, assign: 'category' }
                ],
                payload: {
                    output: 'stream',
                    multipart: { output: "stream" },
                    maxBytes: 10000000000, // 10 GB
                    parse: true,
                    timeout: 600000 // Timeouts within 10 minutes
                },
                validate: {
                    payload: {
                        file: Joi.object().required(),
                        crumb: Joi.string(),
                        storeId: Joi.number().min(0).required()
                    }
                },
                plugins: {
                    // @ts-ignore
                    crumb: false
                },
                description: 'File-Upload endpoint'
            },
            handler: async (request) => await uploadLogic.conductUpload(request)
        });

        server.route({
            method: 'POST',
            path: '/fileDownload',
            options: {
                validate: {
                    payload: {
                        url: Joi.string().uri().required(),
                        storeId: Joi.number().min(0).required(),
                        crumb: Joi.string()
                    },
                    options: {
                        allowUnknown: false
                    }
                },
                description: 'File-Download endpoint'
            },
            handler: async (request) => await downloadLogic.conductDownload(request)
        });

    }
}

export default plugin;
