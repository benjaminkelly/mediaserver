import { Plugin, Server } from '@hapi/hapi';
import { Categories } from '../../Order/Constants';

const plugin: Plugin<null> = {
    name: 'Category Routes',
    version: '1.0.0',
    async register(server: Server) {
        server.route({
            method: 'GET',
            path: '/categories',
            handler: () => Categories.map((category, index) => {return { ...category, id: index }}),
            options: {
                description: 'Lists all categories'
            }
        });
    }
}

export default plugin;