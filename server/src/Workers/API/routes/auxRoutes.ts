import { Plugin, Server } from '@hapi/hapi';

const plugin: Plugin<null> = {
    name: 'Auxiliary Routes',
    version: '1.0.0',
    register(server: Server) {
        server.route({
            method: 'GET',
            path: '/crumb',
            options: {
                plugins: {
                    // @ts-ignore
                    crumb: {
                        restful: true
                    }
                },
                auth: false,
                handler: async (request, h) => {
                    return {crumb: server.plugins["crumb"].generate(request, h)}
                },
                description: 'Generates a crumb token'
            }
        });
    }
}

export default plugin;