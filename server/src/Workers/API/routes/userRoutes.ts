import { Plugin, Server } from '@hapi/hapi';
import Joi from 'joi';
import RouteLogic from '../routeLogic';

interface PluginOptions {
    routeLogic: RouteLogic;
}

const plugin: Plugin<PluginOptions> = {
    name: 'User Routes',
    version: '1.0.0',
    register(server: Server, options: PluginOptions) {
        const { routeLogic } = options;

        server.route({
            method: 'GET',
            path: '/user',
            handler: async (request) => routeLogic.getUser(request),
            options: {
                description: 'Gets the logged in user'
            }
        });

        server.route({
            method: 'PATCH',
            path: '/user',
            handler: async (request) => routeLogic.modifyUser(request),
            options: {
                validate: {
                    payload: {
                        id: Joi.number().min(0).required(),
                        name: Joi.string().min(1).optional(),
                        password: Joi.string().allow('').optional(),
                        realmId: Joi.number().optional()
                    },
                    options: {
                        allowUnknown: false
                    }
                },
                description: 'Edits the logged in user'
            }
        });

        server.route({
            method: 'POST',
            path: '/join',
            handler: async (request) => await routeLogic.join(request),
            options: {
                validate: {
                    payload: {
                        username: Joi.string().required(),
                        password: Joi.string().allow('').optional()
                    }
                },
                auth: {
                    mode: 'try'
                },
                description: 'Authentication route'
            }
        });

        server.route({
            method: 'POST',
            path: '/leave',
            handler: async (request) => await routeLogic.leave(request),
            options: {
                description: 'Logout route'
            }
        });

        server.route({
            method: 'GET',
            path: '/register',
            handler: async (request, h) => await routeLogic.register(request, h),
            options: {
                validate: {
                    query: {
                        email: Joi.string().email().required(),
                        uuid: Joi.string().uuid({ version: 'uuidv4' }).required()
                    }
                },
                description: 'Sends E-Mail registrations'
            }
        });

        server.route({
            method: 'GET',
            path: '/confirm',
            handler: async (request, h) => await routeLogic.confirmRegistration(request, h),
            options: {
                validate: {
                    query: {
                        uuid: Joi.string().uuid({ version: 'uuidv4' }).required()
                    }
                },
                description: 'Account confirmation address (sent via e-mail)'
            }
        });

        server.route({
            method: 'GET',
            path: '/reset',
            handler: async () => {
                return 'Pending release';
            },
            options: {
                description: 'Pending release'
            }
        });

    }
}

export default plugin;