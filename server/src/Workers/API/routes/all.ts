export { default as RealmRoutes }  from './realmRoutes';
export { default as FileRoutes } from './fileRoutes';
export { default as UserRoutes } from './userRoutes';
export { default as CategoryRoutes } from './categoryRoutes';
export { default as StoreRoutes } from './storeRoutes';
export { default as AuxRoutes } from './auxRoutes';