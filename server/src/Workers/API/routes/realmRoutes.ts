import Hapi, { Server } from '@hapi/hapi';
import Joi from 'joi';
import RouteLogic from '../routeLogic';

interface PluginOptions {
    routeLogic: RouteLogic;
}

const plugin: Hapi.Plugin<PluginOptions> = {
    name: "Realm Routes",
    version: "1.0.0",
    register(server: Server, options: PluginOptions) {
        const { routeLogic } = options;
        server.route({
            method: 'GET',
            path: '/realm',
            handler: async () => await routeLogic.getRealms(),
            options: {
                description: 'Lists all realms'
            }
        });

        server.route({
            method: 'PUT',
            path: '/realm',
            handler: async (request) => await routeLogic.assignRealm(request),
            options: {
                description: 'Assigns a specific realm to an authenticated user',
                validate: {
                    payload: {
                        realmId: Joi.number().integer().min(0).required(),
                        password: Joi.string().allow(null).allow("").optional()
                    }
                },
            }
        });
    }
}

export default plugin;