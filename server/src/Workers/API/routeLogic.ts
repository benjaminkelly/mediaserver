import { Lifecycle, Request, ResponseToolkit } from "@hapi/hapi";
import * as Mailer from '../Mailer';
import Boom from '@hapi/boom';
import * as Response from '../Transmission/Response';
import UserHandler from "../Security/UserHandler";
import { getRepository, In } from "typeorm";
import TokenModel from "../../DatabaseModel/models/token";
import RegistrationModel from "../../DatabaseModel/models/registration";
import Realm from "../../DatabaseModel/models/realm";
import Store from "../../DatabaseModel/models/store";
import User from "../../DatabaseModel/models/user";

class RouteLogic {

    public async register(request: Request, h: ResponseToolkit): Promise<Lifecycle.ReturnValue> {
        if (request.query.email && request.query.uuid) {
            let user: User;
            let email = request.query.email;
            let uuid = request.query.uuid;
            let registration = await getRepository(RegistrationModel).findOne({
                where: {
                    uuid: uuid
                }
            });
            if (registration) {
                let id = registration.user.id;
                user = await getRepository(User).findOne({
                    where: {
                        id: id,
                        email: email
                    }
                });
            }
            if (user) {
                let userName = user.name;
                let userEmail = user.email;
                let link = `https://${request.info.host}/confirm?uuid=${uuid}`
                await Mailer.sendVerification(userName, userEmail, link);
                return link;
            }
            return Boom.internal();
        } else {
            return h.redirect('/login');
        }
    }

    public async confirmRegistration(request: Request, h: ResponseToolkit) {
        let registrationUuid: string;
        registrationUuid = request.query.uuid;
        let token = await UserHandler.approveRegistration(registrationUuid);
        if (token) {
            // @ts-ignore
            request.cookieAuth.set({ token: token });
        }
        return h.redirect('/gallery');
    }

    public async join(request: Request) {
        let httpBody = request.payload;
        let username = httpBody['username'];
        let password = (httpBody['password'] != null) ? httpBody['password'] : '';

        let { error, token } = await UserHandler.login(username, password);
        if (error) {
            return Response.invalidCredentials();
        } else {
            // @ts-ignore
            request.cookieAuth.set({ token: token });
            return Response.greet(username, token);
        }
    }

    public async getUser(request: Request) {
        if (request.auth.artifacts && request.auth.artifacts.token && typeof request.auth.artifacts["token"] === "string") {
            let token: string = request.auth.artifacts["token"];
            let userFields = await UserHandler.getFields(token);
            if (userFields.error === null) {
                userFields.storeIds = null;
                return userFields;
            }
        }
        else {
            return Boom.unauthorized('No token provided.');
        }
        return Boom.forbidden();
    }

    public async modifyUser(request: Request) {
        let payload = request.payload as {
            id: number,
            name?: string,
            password?: string,
            realmId?: number
        };
        if (request.auth.artifacts && request.auth.artifacts.token && typeof request.auth.artifacts["token"] === "string") {
            let token: string = request.auth.artifacts["token"];
            let userFields = await UserHandler.getFields(token);
            delete userFields.verified;
            delete userFields.storeIds;
            if (userFields.error === null && payload.id === userFields.id) {
                delete userFields.error;
                let userRepo = getRepository(User);
                let user = await userRepo.findOne(userFields.id);
                if (user) {
                    if (payload.password) {
                        user.password = payload.password;
                        await user.hashPassword();
                        delete payload.password;
                    }
                    await userRepo.save({
                        ...user,
                        ...payload
                    });
                    return true;
                }
            }
        }
        else {
            return Boom.unauthorized('No token provided.');
        }
        return Boom.forbidden();
    }

    public async getRealms() {
        let realms = await getRepository(Realm).find();
        realms = realms.filter((realm) => realm.hidden === false)
        return realms.map((realm) => {
            return {
                id: realm.id, name: realm.name, description: realm.description,
                protected: (realm.password !== "")
            }
        });
    }

    public async assignRealm(request: Request): Promise<Lifecycle.ReturnValue> {
        let token = "";
        if (typeof request.auth.artifacts?.token === "string") {
            token = request.auth.artifacts?.token;
        }
        let realmId: number = request.payload["realmId"];

        const { verified, id } = await UserHandler.getFields(token);

        if (verified) {
            let realmRepo = getRepository(Realm);
            let realm = await realmRepo.findOne(realmId);
            if (realm) {
                if (realm.password !== null) {
                    if (request.payload["password"] !== null) {
                        let password = request.payload["password"];
                        if (await realm.isAuthorised(password) === false) {
                            return Boom.unauthorized();
                        }
                    }
                }
                let userRepo = getRepository(User);
                await userRepo.update({
                    id: id
                }, {
                    realm: realm
                });
                return 'linkedRealm';
            } else {
                return Boom.notFound();
            }
        } else {
            return Boom.unauthorized();
        }
    }

    public async getStores(request: Request) {
        // @ts-ignore
        let storeIds: number[] = request.auth.credentials.storeIds;
        let storeRepo = getRepository(Store);
        return await storeRepo.find({
            where: {
                id: In(storeIds)
            }
        });
    }

    public async leave(request: Request) {
        // @ts-ignore
        const { error, name } = await UserHandler.logout(request.auth.artifacts?.token);
        if (error) {
            return Response.invalidToken();
        } else {
            // @ts-ignore
            request.cookieAuth.clear();
            return Response.farewell(name);
        }
    }

    public async isRegistered(tokenValue: string | null) {
        let tokenPresent = false;
        let username = null;
        if (tokenValue != null) {
            tokenPresent = true;
            let token = await getRepository(TokenModel).findOne({
                relations: ["user"],
                where: {
                    value: tokenValue
                }
            });
            if (token && token.user) {
                username = token.user.name;
            }
        }
        return { tokenPresent, username };
    }
}

export default RouteLogic;
