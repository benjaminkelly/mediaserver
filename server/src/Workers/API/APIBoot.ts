import Hapi from "@hapi/hapi";
import * as HapiCookie from '@hapi/cookie';
import * as HapiCrumb from '@hapi/crumb';
import * as Inert from '@hapi/inert';
import CatboxRedis from "@hapi/catbox-redis";
import { MEDIAPATH, RedisPrefixes } from "../Order/Constants";
import IORedis from "ioredis";
import Joi from "joi";
import fs from "fs";
import path from "path";
import HapiNowAuth from '@now-ims/hapi-now-auth';
import Blipp from 'blipp';
import password from 'generate-password';
import { Server } from 'socket.io';
import API from './API';
import RouteLogic from "./routeLogic";
import UserHandler from "../Security/UserHandler";

export default async function apiBoot(io: Server): Promise<Hapi.Server> {

    const routeLogic = new RouteLogic();
    let httpsOptions: any = false;
    let config = null;

    try {
        config = require("../../../config/default");
        try {
            httpsOptions = {
                key: fs.readFileSync(path.join(__dirname, '../../../', 'Certificates', config.TLS.key)),
                cert: fs.readFileSync(path.join(__dirname, '../../../', 'Certificates', config.TLS.cert))
            }
        } catch (e) {
            console.info("No https certificates provided, see docs to find out more.");
        }
    } catch (e) {
        // ignore
    }

    const server = Hapi.server({
        port: 8080,
        routes: {
            files: {
                relativeTo: MEDIAPATH
            },
            auth: {
                strategies: ["session", "bearerToken"]
            }
        },
        compression: {
            minBytes: 4096
        },
        state: {
            strictHeader: true,
            ignoreErrors: false,
            isSecure: true,
            isSameSite: 'Lax'
        },
        cache: {
            provider: {
                constructor: CatboxRedis,
                options: {
                    // @ts-ignore
                    client: new IORedis()
                }
            }
        },
        tls: httpsOptions
    });

    server.validator(Joi);

    await server.register(HapiNowAuth);
    await server.register(Blipp);
    // @ts-ignore
    await server.register(Inert);
    // @ts-ignore
    await server.register(HapiCookie);

    const crumbOptions: HapiCrumb.RegisterOptions = {
        enforce: false,
        logUnauthorized: true,
        cookieOptions: {
            isSecure: true,
            strictHeader: true,
            ignoreErrors: false,
            isSameSite: 'Strict'
        }
    }
    await server.register({
        // @ts-ignore
        plugin: HapiCrumb,
        options: crumbOptions
    });

    server.auth.strategy("bearerToken", "hapi-now-auth", {
        verifyJWT: false,
        validate: UserHandler.proveIdentity
    });
    
    let cookiePassword = '';
    if (process.env.NODE_ENV === "production") {
        cookiePassword = password.generate({ length: 100, symbols: true, lowercase: true, uppercase: true, numbers: true })
    }
    else {
        cookiePassword = 'jvTSOGXXFswjwTZUydegnXMFC6XskUBRuxkBNrFmJUXAEz1l4nXBZy5ZSQyUesA2yhKijF6jnzKZHr73C534zvNsgSfHQuNCS09d';
    }
    let redis = new IORedis();
    redis.set(RedisPrefixes.cookiePw, cookiePassword);
    server.auth.strategy('session', 'cookie', {
        cookie: {
            name: 'token',
            ttl: 1000 * 60 * 30,
            clearInvalid: true,
            path: '/',
            isSameSite: 'Lax',
            isSecure: true,
            isHttpOnly: true,
            password: cookiePassword
        },
        keepAlive: true,
        validateFunc: UserHandler.proveIdentity,
        redirectTo: false
    });
    server.auth.default('session');

    await server.register({
        plugin: API,
        options: {
            io, routeLogic
        }
    }, {
        routes: {
            prefix: '/api'
        }
    });

    await server.start();
    return server;
}
