import SocketIO from "socket.io";
import Hapi, {Server} from '@hapi/hapi';
import RouteLogic from "./routeLogic";
import { FileRoutes, RealmRoutes, UserRoutes, CategoryRoutes, StoreRoutes, AuxRoutes } from './routes/all';

interface PluginOptions {
    io: SocketIO.Server;
    routeLogic: RouteLogic;
}

const plugin: Hapi.Plugin<PluginOptions> = {
    name: "API",
    version: "1.0.0",
    async register(server: Server, options: PluginOptions) {
        const { io, routeLogic } = options;

        await server.register({
            plugin: RealmRoutes,
            options: {
                routeLogic
            }
        });

        await server.register({
            plugin: FileRoutes,
            options: {
                io, routeLogic
            }
        });

        await server.register({
           plugin: UserRoutes,
           options: {
               routeLogic
           } 
        });

        await server.register({
            plugin: CategoryRoutes
        });

        await server.register({
            plugin: StoreRoutes,
            options: {
                routeLogic
            }
        });

        await server.register({
            plugin: AuxRoutes
        });
    }
}

export default plugin;
