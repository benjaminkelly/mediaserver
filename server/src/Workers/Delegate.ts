import SessionManager from "./Socket/Session";
import DashBoardManager from "./Socket/DashBoard";
import cookieParser from 'cookie-parser';
import Redis from 'ioredis';
import { Server as SocketIO } from "socket.io";
import { createAdapter } from 'socket.io-redis';
import apiBoot from "./API/APIBoot";
import SocketBinder from "./Socket/SocketBinder";
import { grey } from "colors/safe";
const redisClient = new Redis();
const pubClient = redisClient.duplicate();
const subClient = redisClient.duplicate();

export default class Delegator {
    public async invoke() {
        const io = new SocketIO({
            cors: {
                origin: "*",
                credentials: true
            }
        });

        io.adapter(createAdapter({ pubClient, subClient }));

        io.use((socket, next) => {
            let parser = cookieParser.apply(null, {});
            parser(socket.request, null, next);
        });
        //
        //// Socket helpers
        //
        SocketBinder.init(io);
        SessionManager.init(io, redisClient);
        DashBoardManager.init(io);

        io.on("connect", (session) => {
            session.on("message", (message) => {
                console.log(grey('==='));
                console.log(`Message by id ${session.id}: `);
                console.log(message);
                console.log(grey('==='));
            });
        });
        //
        //// API routes
        //
        const server = await apiBoot(io);

        io.listen(server.listener);

        console.log(`Direct API traffic to port 8080`);
    }
}
