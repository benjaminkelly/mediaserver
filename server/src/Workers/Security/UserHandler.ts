import {getManager, getRepository} from "typeorm";
import User, {UserValues} from "../../DatabaseModel/models/user";
import Registration from "../../DatabaseModel/models/registration";
import Token from "../../DatabaseModel/models/token";
import { RedisPrefixes } from "../Order/Constants";
import IORedis from "ioredis";
import Iron from "@hapi/iron";

export default class UserHandler {

    private static redisClient = new IORedis();

    public static async proveIdentity(request, session) {
        let token: string;
        if (typeof session === "object" && session.token != null) {
            token = session.token;
        } else {
            token = session;
        }
        const {name, id, storeIds, verified, error} = await UserHandler.getFields(token);
        if (error == null) {
            return {credentials: {id, name, storeIds, verified}, valid: true, isValid: true, artifacts: {token}}
        }
        return {credentials: {}, valid: false, isValid: false}
    }

    static async getFields(tokenValue?: string): Promise<UserValues> {
        let tokenRepo = getRepository(Token);
        let error = null;
        let cachedUser: UserValues = await User.probeCache(tokenValue);
        if (cachedUser) {
            return cachedUser;
        }
        else {
            let token = await tokenRepo.findOne({
                relations: ["user"],
                where: {
                    value: tokenValue
                }
            });
            if (token != null && token.user != null) {
                let user = token.user;
                return await User.persistInCacheAndGetValues(tokenValue, user);
            } else {
                error = "Invalid token.";
                return {error};
            }
        }
    }

    public static async register(name: string, password: string, email: string) {
        let success = false;
        let response: string, uuid: string;
        if (password.length !== 0 && password.length < 8) {
            response = "The password must consist of at least 8 characters or more.";
            return {success, response};
        }

        let user = await getRepository(User).findOne({where: {name: name}});

        if (user) {
            response = "This user already exists.";
        } else {
            user = getRepository(User).create({
                name: name,
                password: password,
                email: email
            });
            await user.save();
            let registration = getRepository(Registration).create({
                user: user
            });
            await registration.save();
            uuid = registration.uuid;
            success = true;
            response = `Welcome ${name}`;
        }
        return {success, response, uuid};
    }

    public static async approveRegistration(uuid) {

        let registration = await getRepository(Registration).findOne({where: {uuid: uuid}});

        if (registration) {
            let userID = registration.user.id;
            let user = await getRepository(User).findOne(userID);

            if (user) {
                await getManager().delete(Registration, registration);
                getRepository(User).update({ verified: true }, { id: userID });

                let token = await getRepository(Token).create({
                    user: user
                })
                return token.value;
            }
        }
        return false;
    }

    public static async login(username, password) {
        let loginError = null;
        let token = null;
        let tokenRepo = getRepository(Token);

        let user = await getRepository(User).findOne({where: {name: username}});
        if (!user) {
            loginError = `Username or password is incorrect.`;
        } else if (!await user.isAuthorised(password)) {
            loginError = "Username or password is incorrect.";
        } else if (user.verified === false) {
            loginError = 'Account is not verified. Please check your e-mails and try again.';
        } else {
            console.log(username + " joined");
            let tokenRow = await tokenRepo.save({
                user: user
            }).catch((e) => loginError = e);
            token = tokenRow.value;
        }
        return {error: loginError, token: token};
    }

    public static async logout(token?: string) {

        const {error, name} = await UserHandler.getFields(token);
        if (!error) {
            console.log(`${name} left`);
            await getRepository(Token).delete({ value: token });
        }
        return {error, name};
    }

    public static async extractTokenFromCookies(cookies?: string): Promise<string | null> {
        let token = null;
        if (cookies) {
            let encryptedCookie = this.findCookieValue(cookies, 'token');
            token = this.extractTokenFromCookie(encryptedCookie);
        }
        return token;
    }

    public static async extractTokenFromCookie(encryptedCookie?: string): Promise<string | null> {
        let token = null;
        if (encryptedCookie) {
            let decryptKey = await this.redisClient.get(RedisPrefixes.cookiePw);
            let decryptedCookie = await Iron.unseal(encryptedCookie, decryptKey, Iron.defaults);
            if (decryptedCookie && decryptedCookie["token"]) {
                token = decryptedCookie["token"];
            }
        }
        return token;
    }

    private static findCookieValue(cookiesString: string, name: string): string | null {
        let value = null;
        if (cookiesString) {
            let cookies = cookiesString.split('; ');
            let cookie = cookies.find((cookie) => cookie.startsWith(name + '='));
            
            if (cookie) {
                let keyValue = cookie.split('=');
                if (keyValue.length === 2) {
                    value = keyValue[1];
                }
            }
        }
        return value;
    }

}
