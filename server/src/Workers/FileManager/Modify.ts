import SocketIO from "socket.io";
import path from 'path';
import fse from 'fs-extra';
import { Categories, FileNameEnum, MEDIAPATH } from '../Order/Constants';
import { Lifecycle, Request } from "@hapi/hapi";
import { getRepository } from "typeorm";
import GalleryFile from "../../DatabaseModel/models/galleryfile";
import Boom from "@hapi/boom";
import Store from "../../DatabaseModel/models/store";

export default class ModificationManager {

    public static async modifyFile(request: Request, io: SocketIO.Server): Promise<Lifecycle.ReturnValue> {
        // obtain the socket session
        let cookies = request.state;
        let socketId = '';
        if (cookies.token && cookies.token["token"]) {
            socketId = cookies.token["token"];
        }
        // get a list of stores the user is apart of
        // @ts-ignore
        let storeIds: number[] = request.auth.credentials.storeIds;
        const fileId = request.params["id"];
        const fileRepo = getRepository(GalleryFile);
        // find the source file
        const sourceFile = await fileRepo.findOne(fileId);
        if (sourceFile === undefined) {
            return Boom.notFound('The source file does not exist.');
        }
        if (storeIds.includes(sourceFile.storeId) === false) {
            return Boom.forbidden('You are not entitled to modify this file.');
        }
        const category = Categories[sourceFile.categoryId];
        const httpPayload = request.payload;
        let targetName: string = sourceFile.name;
        let targetStoreId: number = sourceFile.store.id;
        let targetStoreName = sourceFile.store.name;
        if (httpPayload["name"]) {
            targetName = httpPayload["name"];
        }
        let store: Store = null;
        if (httpPayload["storeId"]) {
            targetStoreId = parseInt(httpPayload["storeId"]);
            if (storeIds.includes(targetStoreId) === false) {
                return Boom.forbidden('The targeted store is not in your realm.');
            }
            // find the store's name
            let storeRepo = getRepository(Store);
            store = await storeRepo.findOne(targetStoreId);
            if (store === undefined) {
                return Boom.badData('The targeted store does not exist.');
            }
            targetStoreName = store.name;
        }
        let sourceFileLocation = path.join(MEDIAPATH, sourceFile.store.name, category.title, sourceFile.name);
        if (await fse.pathExists(sourceFileLocation) === false) {
            console.log(`The source file could not be found.`);
            return Boom.notFound('The source file could not be found.');
        }
        let preferredFileLocation = path.join(MEDIAPATH, targetStoreName, category.title, targetName);
        if (await fse.pathExists(preferredFileLocation)) {
            io.to(socketId).emit("spawnToast", {
                title: "Target exists already.",
                text: `${targetName}: is reserved for a different file.`,
                type: "warning",
                icon: ""
            });
            return Boom.badData("Cannot overwrite an existing file.");
        }
        await fse.move(sourceFileLocation, preferredFileLocation).catch((reason) => {
            console.error(reason);
            throw reason;
        });
        let sameStore = sourceFile.store.id === targetStoreId;
        let sameName = sourceFile.name === targetName;
        let sourceStore = sourceFile.store.name;
        sourceFile.name = targetName;
        if (store) {
            sourceFile.store = store;
        }
        sourceFile.storeId = targetStoreId;
        await sourceFile.save();
        let thumbnailPath = `/api/files/${sourceFile.id}/${FileNameEnum.thumb}`;
        if (sameStore) {
            if (sameName === false) {
                io.to(`store-${sourceFile.storeId}`).emit("spawnToast", {
                    title: "File renamed.",
                    text: `${sourceFile.name} is now ${targetName}`,
                    type: "info",
                    icon: thumbnailPath
                });
            }
        } else {
            io.to(`store-${sourceFile.storeId}`).emit("spawnToast", {
                title: "File transferred.",
                text: `${sourceFile.name} ${sourceStore} => ${targetStoreName}`,
                type: "info",
                icon: thumbnailPath
            });
        }
        io.to(`store-${sourceFile.storeId}`).emit("fileTransferred", httpPayload);
        return true;
    }
}
