import { blue } from 'colors/safe';
import fse, { pathExists } from 'fs-extra';
import path from 'path';
import { Categories, MEDIAPATH } from '../Order/Constants';
import SocketIO from "socket.io";
import { Lifecycle, Request } from '@hapi/hapi';
import Boom from '@hapi/boom';
import { getRepository } from 'typeorm';
import GalleryFile from '../../DatabaseModel/models/galleryfile';

export default class DeletionManager {

    public static async deleteFile(request: Request, io: SocketIO.Server): Promise<Lifecycle.ReturnValue> {

        const fileId = request.params["id"];
        let fileRepo = getRepository(GalleryFile);
        let file = await fileRepo.findOne(fileId);
        // @ts-ignore
        let userStoreIds: number[] = request.auth.credentials.storeIds;
        if (file && userStoreIds.includes(file.storeId)) {
            // remove from db
            await file.remove();
            io.to(`store-${file.store.id}`).emit('removeFile', file.id);
            console.log(blue(`Deleted: ${file.name}`));
            const category = Categories[file.categoryId];
            let completePackagePath = path.join(MEDIAPATH, file.store.name, category.title, file.name);
            if (await pathExists(completePackagePath)) {
                // remove from disk
                await fse.remove(completePackagePath).catch((error) => {
                    console.error(error);
                    return Boom.internal();
                });
                return true;
            }
        }
        return Boom.notFound();
    }
}