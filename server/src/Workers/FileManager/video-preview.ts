// Courtesy of tpkn

// To Clarify: This is not the original

// Some modifications were necessary

/*!
 * Video Preview, http://tpkn.me/
 */
import fse from 'fs-extra';
import path from 'path';
import ffmpeg from 'fluent-ffmpeg';

export enum OutputFormat {
    aPng = "apng",
    Gif = "gif",
    Webp = "webp"
}

type OptionsType = {
    source: string,
    destinationDir: string,
    name: string,
    outputFormat?: OutputFormat,
    width, height: number
    fps, totalFrames: number
    cleanup?: boolean
    framesFormat?: string
    tempDir?: string
}

export default async function videoPreview(options: OptionsType) {

    try {
        options = { framesFormat: 'png', outputFormat: OutputFormat.Gif, fps: 1, tempDir: path.join(path.parse(options.source).dir, `/temp_${uniqid()}`), cleanup: true, ...options }
        // Define preview folder path if it's not set
        if (!options.destinationDir) {
            options.destinationDir = `${options.tempDir}`;
        }
        // Make sure that temporary folder exists
        await fse.ensureDir(options.tempDir);
        await cutIntoFrames(options);
        await glueFramesTogether(options);
    } catch (error) {
        await stop(options);
        throw error;
    }
}

async function cutIntoFrames(options: OptionsType) {
    return new Promise<null>((resolve, reject) => {
        // cut the video into individual images
        getVideoDuration(options.source).then((durationInSeconds) => {
            // 1/frameIntervalInSeconds = 1 frame each x seconds
            const frameIntervalInSeconds = Math.floor(
                durationInSeconds / options.totalFrames
            );
            ffmpeg(options.source)
                .outputOptions([`-vf fps=1/${frameIntervalInSeconds},scale=${options.width}:${options.height}`])
                .output(`${options.tempDir}/frame%04d.${options.framesFormat}`)
                .on('error', reject)
                .on('end', resolve)
                .run();
        }).catch(reject);
    });
}

async function glueFramesTogether(options: OptionsType) {
    return new Promise<void>((resolve, reject) => {
        // Glue those frames together
        let inputOptions = [];
        let outputOptions = [];
        switch (options.outputFormat) {
            case OutputFormat.aPng:
                inputOptions.push("-f apng");
                outputOptions.push("-plays 0");
                break;
            case OutputFormat.Gif:
                inputOptions.push("-f image2");
                break;
            case OutputFormat.Webp:
                inputOptions.push("-f image2");
                outputOptions.push("-c:v libvpx-vp9");
                outputOptions.push("-pix_fmt yuva420p");
                break;
        }
        inputOptions.push(`-framerate ${options.fps}`);
        ffmpeg(path.join(options.tempDir, `frame%04d.${options.framesFormat}`))
            .inputOptions(inputOptions)
            .outputOptions(outputOptions)
            .output(`${options.destinationDir}/${options.name}.${options.outputFormat}`)
            .on('error', reject)
            .on('end', async () => {
                stop(options);
                resolve();
            })
            .run();
    });
}

function uniqid(): string {
    return (Math.random() + Math.random() + Math.random() + Math.random() + Math.random() + Date.now()).toString(36).replace('.', '');
}

async function getVideoDuration(input: string) {
    return new Promise<number>((resolve, reject) => {
        ffmpeg.ffprobe(input, (error, data) => {
            if (error) reject(error);
            if (isNaN(Number(data.streams[0].duration))) {
                reject("Could not determine video duration.");
            }
            resolve(parseInt(data.streams[0].duration));
        });
    });
}

async function stop(options) {
    if (options.cleanup) {
        await fse.remove(options.tempDir);
    }
}
