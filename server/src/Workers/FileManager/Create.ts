import path from 'path';
import {Categories, CategoryEnum, FileNameEnum, PUBLICDIR} from '../Order/Constants';
import {imageSize} from "image-size";

import sharp from 'sharp';
import hbjs from "handbrake-js";
import VideoPreview, {OutputFormat} from './video-preview';
import ffmpeg from 'fluent-ffmpeg';
import fs from 'fs';
import fse from 'fs-extra';
import {red, grey, green} from "colors/safe";
import {TypedEmitter} from "tiny-typed-emitter";
import GalleryFile from "../../DatabaseModel/models/galleryfile";

declare interface CreateEvents {
    "progressBegin": (file: GalleryFile) => void;
    "progress": (percent: number, file: GalleryFile) => void;
    "progressEnd": (file: GalleryFile) => void;
    "uploadFinished": (file: GalleryFile) => void;
    "finished": (file: GalleryFile) => void;
}

export default class CreationManager extends TypedEmitter<CreateEvents> {

    private static instance: CreationManager = null

    /**
     * Please use CreationManager.getInstance() instead
     * @private
     */
    private constructor() {
        super();
    }

    public static getInstance(): CreationManager {
        if (this.instance == null) {
            this.instance = new CreationManager();
        }
        return this.instance;
    }

    public async process(file: GalleryFile, destination?: path.ParsedPath): Promise<GalleryFile> {
        const { categoryId, store } = file;
        const category = Categories[categoryId];
        if (destination == null) {
            destination = path.parse(path.join(PUBLICDIR, "media", store.name, category.title, file.name, "Raw" + file.extension));
        }
        let inspectionFile = path.join(destination.dir, '/.inspected');
        const pathDoesExist = await fse.pathExists(inspectionFile);
        if (pathDoesExist === false) {
            switch (category.title) {
                case CategoryEnum.IMAGES:
                    file = await this.imageUpload(file, destination);
                    break;
                case CategoryEnum.VIDEOS:
                    file = await this.videoUpload(file, destination);
                    break;
                default:
                    throw new Error("File Format isn't supported yet: " + destination.ext);
            }
        } else {
            return file;
        }
        await fse.ensureFile(inspectionFile);
        return file;
    }

    public async imageUpload(file: GalleryFile, inputFile: path.ParsedPath): Promise<GalleryFile> {
        let imageThumbPath = path.join(inputFile.dir, FileNameEnum.thumb + ".png");
        this.shrinkToThumbnail(path.format(inputFile), imageThumbPath, 300, 300, () => {
            this.emit("uploadFinished", file);
            this.emit("finished", file);
        });
        return file;
    }

    public async videoUpload(file: GalleryFile, inputFile: path.ParsedPath): Promise<GalleryFile> {
        const category = Categories[file.categoryId];
        if (category.acceptedExts.includes(file.extension.toLowerCase()) === false) {
            const { galleryFile, outputFile } = await this.transcodeVideo(file, inputFile)
            if (outputFile !== null) {
                inputFile = outputFile;
            } else {
                throw new Error(`Unable to transcode ${file.name}`);
            }
            file = galleryFile;
        }
        await this.finalizeVideoUpload(file, inputFile);
        return file;
    }

    private progressReport(lastKnownPercent: number, progress: number, file: GalleryFile): number {
        if (Math.floor(progress) % 5 === 0) {
            if (Math.floor(progress) % 10 === 0) {
                if (Math.floor(progress) !== lastKnownPercent) {
                    lastKnownPercent = Math.floor(progress);
                    console.log(lastKnownPercent + "%", file.name);
                }
            }
            this.emit("progress", progress, file);
        }
        return lastKnownPercent;
    }

    public async transcodeVideo(file: GalleryFile, inputFile: path.ParsedPath): Promise<{ galleryFile: GalleryFile, outputFile?: path.ParsedPath }> {
        return new Promise<{ galleryFile: GalleryFile, outputFile?: path.ParsedPath }>((resolve, reject) => {
            let lastKnownPercent = 0;
            let outputFile: path.ParsedPath = {...inputFile};
            const {categoryId} = file;
            const category = Categories[categoryId];
            outputFile.ext = category.convertTo;
            outputFile.base = outputFile.name + outputFile.ext;
            file.extension = outputFile.ext;

            console.log("Transcoding: " + file.name);

            this.emit("progressBegin", file);

            if (inputFile.ext.toLowerCase() === '.avi') {
                ffmpeg(path.format(inputFile), {
                    timeout: 300,
                    niceness: 10
                }).videoCodec('libx264').toFormat('mp4')
                    .on("error", async (err) => {
                        await fse.remove(path.format(inputFile));
                        reject(err);
                    })
                    .on("progress", (progress) => {
                        lastKnownPercent = this.progressReport(lastKnownPercent, progress.percent, file);
                    })
                    .on("end", () => {
                        fs.unlink(path.format(inputFile), async (error) => {
                            if (error) reject(error);
                            resolve({outputFile, galleryFile: file});
                        });
                    })
                    .output(path.format(outputFile)).run();
            } else {
                let options = {
                    input: path.format(inputFile),
                    output: path.format(outputFile),
                    preset: 'Very Fast 720p30'
                }
                hbjs.spawn(options)
                    .on("error", async (err) => {
                        await fse.remove(path.format(inputFile)).catch(reject);
                        reject(err);
                    })
                    .on("progress", progress => {
                        lastKnownPercent = this.progressReport(lastKnownPercent, progress.percentComplete, file);
                    })
                    .on("end", async () => {
                        this.emit("progressEnd", file);
                        await fse.remove(path.format(inputFile)).catch(reject);
                        resolve({outputFile, galleryFile: file});
                    });
            }
        });
    }

    private async finalizeVideoUpload(file: GalleryFile, inputFile: path.ParsedPath) {
        return new Promise<void>((resolve, reject) => {
            console.log("Finalizing: " + file.name);
            let icon = FileNameEnum.thumb + ".png";
            let videoPath = path.format(inputFile);
            try {
                ffmpeg()
                    .on('error', async (e) => {
                        await fse.remove(path.format(inputFile));
                        return reject(e);
                    })
                    .input(videoPath)
                    .screenshots({
                        timestamps: ['50%'],
                        size: '300x275',
                        folder: inputFile.dir,
                        filename: icon
                    })
                    .on('end', async (err) => {
                        if (err) return reject(err);
                        console.log("Snapped thumbnail");
                        this.emit("uploadFinished", file);

                        // Generate the gif after notifing the user
                        console.log(grey("Compiling gif..."))
                        await VideoPreview({
                            source: videoPath,
                            name: FileNameEnum.preview,
                            destinationDir: inputFile.dir,
                            outputFormat: OutputFormat.Gif,
                            width: 300,
                            height: 275,
                            fps: 1,
                            totalFrames: 10
                        }).then(() => {
                            console.log(green("Gif compiled!"));
                            this.emit('finished', file);
                            resolve();
                        })
                            .catch((reason) => {
                                console.error(red("Gif failed to compile!"), grey(reason));
                                reject(reason);
                            });
                    });
            } catch (e) {
                reject(e);
            }

        });
    }

    private shrinkToThumbnail(source, destination, overrideWidth, overrideHeight, callback) {
        imageSize(source, (errors, dimensions) => {
            if (errors) {
                console.error("sizeOf Exception > Create.ts: " + errors);
                return;
            }
            let width = dimensions.width;
            let height = dimensions.height;
            let newHeight = height;
            let newWidth = width;

            if (width >= 300 || height >= 300) {
                if (width > height) {
                    newHeight = 275;
                    newWidth = 300;
                } else if (height > width) {
                    newHeight = 300;
                    newWidth = 275;
                } else {
                    newHeight = 300;
                    newWidth = 300;
                }

                if (overrideHeight != null) {
                    newHeight = overrideHeight;
                }
                if (overrideWidth != null) {
                    newWidth = overrideWidth;
                }
            }

            sharp(source)
                .resize(newWidth, newHeight, {
                        fit: "cover"
                    }
                )
                .toFile(destination, (err) => {
                    if (err) {
                        console.error("toFile Exception > Create.ts: " + err);
                    }
                    callback(newWidth, newHeight);
                });
        });
    }
}

