import path from 'path';
import fse from 'fs-extra';
import glob from 'fast-glob';
import slash from "../../utils/slash";
import * as Response from '../Transmission/Response';
import {Categories, Category, CategoryTitles, FileNameEnum, MEDIAPATH} from "../Order/Constants";
import { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';
import { getRepository, In } from 'typeorm';
import Store from '../../DatabaseModel/models/store';
import Boom from '@hapi/boom';
import GalleryFile from '../../DatabaseModel/models/galleryfile';
import { ReplyFileHandlerOptions } from '@hapi/inert';

export type File = {
    name: string,
    includesGif: boolean,
    extension: string,
    thumbnail: boolean
}

export interface FileResponseToolkit extends ResponseToolkit {
    /**
     * Transmits a file from the file system. The 'Content-Type' header defaults to the matching mime type based on filename extension.
     * @see {@link https://github.com/hapijs/inert#replyfilepath-options}
     */
    file(path: string, options?: ReplyFileHandlerOptions): ResponseObject;
}

export default class Gatherer {

    public async getLibrary(request: Request) {
        let data = {categories: <object[]>[]};
        let storeNames: string[];
        let httpQuery = request.query;
        // @ts-ignore
        let storeIds: number[] = request.auth.credentials["storeIds"];
        let categories: Category[] = new Array();
        if (httpQuery['category'] != null && CategoryTitles.includes(httpQuery['category'])) {
            categories.push(Categories.find((category) => category.title === httpQuery['category']));
        } else {
            categories = [...Categories];
        }
        let storeRepo = getRepository(Store);
        let stores = await storeRepo.find({
            where: {
                id: In(storeIds)
            }
        });
        storeNames = stores.map((store) => store.name);
        if (httpQuery['store']) {
            let store = httpQuery['store'];
            if (storeNames.includes(store)) {
                storeNames = [store];
            }
        }

        let categoryIndex = 0;
        for (const category of categories) {
            data.categories.push({title: category.title});
            data.categories[categoryIndex]["stores"] = new Array();
            data.categories[categoryIndex]["container"] = category.htmlContainer;
            for (const store of storeNames) {
                let storeObject = new Object();
                let storeData = await this.collectByCategory(category, store);
                storeObject["title"] = store;
                storeObject["data"] = storeData;
                // insert store into stores
                data.categories[categoryIndex]["stores"].splice(data.categories[categoryIndex]["stores"].length, 0, storeObject);
            }
            categoryIndex++;
        }
        return Response.giveFetched(data);
    }

    public async countMediaFiles() {
        let categoryStats = new Object();

        for (const category of CategoryTitles) {
            let folders = await glob(`${MEDIAPATH}/*/${category}/*`, {
                onlyDirectories: true
            });
            categoryStats[category] = folders.length;
        }
        return categoryStats;
    }

    public async getFiles(request: Request) {
        // @ts-ignore
        let storeIds: number[] = request.auth.credentials.storeIds;
        let intendedStore = request.query["store"];
        let intendedCategory = request.query["category"];
        // if store is within the authorized scope
        if (intendedStore != null && storeIds.includes(intendedStore)) {
            storeIds = [intendedStore];
        }
        let fileRepo = getRepository(GalleryFile);
        if (intendedCategory != null) {
            return await fileRepo.find({
                where: {
                    store: In(storeIds),
                    categoryId: intendedCategory
                },
                loadEagerRelations: false,
                cache: true
            });
        }
        return await fileRepo.find({
            where: {
                store: In(storeIds)
            },
            loadEagerRelations: false,
            cache: true
        });
    }

    public async getFileById(request: Request, response: FileResponseToolkit) {
        let fileRepo = getRepository(GalleryFile);
        let id = request.params.id;
        let fileType = request.params.fileType;
        let mode = request.params.mode;
        let file = await fileRepo.findOne({
            where: {
                id: id
            },
            cache: true
        });

        if (file !== undefined) {
            // @ts-ignore
            let storeIds: number[] = request.auth.credentials.storeIds;
            // if user is authorized
            if (storeIds.includes(file.store.id)) {
                let category = Categories[file.categoryId];
                let folderPath = path.join(file.store.name, category.title, file.name);
                let filePath = "";
                switch (fileType) {
                    case FileNameEnum.raw:
                        if (await fse.pathExists(path.join(MEDIAPATH, folderPath, "Raw" + file.extension))) {
                            filePath = path.join(folderPath, "Raw" + file.extension);
                        } else {
                            let rawFiles = await glob(glob.escapePath(slash(path.join(MEDIAPATH, folderPath, "Raw."))) + category.extensions, {
                                caseSensitiveMatch: false,
                                onlyFiles: true
                            });
                            if (rawFiles.length > 0) {
                                filePath = path.normalize(rawFiles[0].slice(MEDIAPATH.length + 1));
                            }
                        }
                        break;
                    case FileNameEnum.thumb:
                        filePath = path.join(folderPath, "Thumb.png");
                        break;
                    case FileNameEnum.preview:
                        filePath = path.join(folderPath, "Preview.apng");
                        break;
                    default:
                        break;
                }
                return response.file(filePath, {confine: MEDIAPATH, filename: file.name, mode, lookupCompressed: true});
            } else {
                return Boom.unauthorized();
            }
        } else {
            return Boom.notFound();
        }
    }

    public async collect(stores: string[], specificCategory: string = null) {
        let mediaContainer = new Array();
        let categories: Category[] = new Array();
        if (specificCategory !== null) {
            if (CategoryTitles.includes(specificCategory) === false) {
                return mediaContainer;
            }
            categories.push(Categories.find((category) => category.title === specificCategory));
        } else {
            categories = [...Categories];
        }
        for (const category of categories) {
            mediaContainer[category.title] = new Object();
            for (let storeIndex = 0; storeIndex < stores.length; storeIndex++) {
                let store = stores[storeIndex];
                mediaContainer[category.title][store] = new Array();
                mediaContainer[category.title][store] = await this.collectByCategory(category, store);
            }
        }
        return mediaContainer;
    }

    public async collectByCategory(category: Category, store: string): Promise<File[]> {
        const thumbnail = "Thumb.png";
        const gif = "Preview.gif";
        const categoryDirectory = path.join(MEDIAPATH, store, category.title);

        let library = this.fetchPackages(categoryDirectory);
        let reviewedLibrary = new Array();

        for (let fileIndex = 0; fileIndex < library.length; fileIndex++) {
            let filePackage = library[fileIndex];
            let passedExtension = null;
            let passingThumbnail = false;
            let passingGif = false;

            let packageDir = path.join(categoryDirectory, filePackage);


            packageDir = glob.escapePath(slash(path.join(packageDir, "Raw.")));
            let rawFile = await glob(packageDir + category.extensions, {caseSensitiveMatch: false});
            if (rawFile.length === 1) {
                passedExtension = path.parse(rawFile[0]).ext;
            } else {
                continue;
            }

            if (await fse.pathExists(path.join(packageDir, thumbnail))) {
                passingThumbnail = true;
            }

            if (await fse.pathExists(path.join(packageDir, gif))) {
                passingGif = true;
            }

            reviewedLibrary.push({
                name: filePackage,
                includesGif: passingGif,
                extension: passedExtension,
                thumbnail: passingThumbnail
            });
        }
        return reviewedLibrary;
    }

    private fetchPackages(parentDirectory: string) {
        if (!fse.pathExistsSync(parentDirectory)) {
            return [];
        } else {
            return fse.readdirSync(parentDirectory, {withFileTypes: true})
                .filter((item) => item.isDirectory())
                .map((packageProfile) => {
                    return {
                        name: packageProfile.name,
                        time: fse.statSync(`${parentDirectory}/${packageProfile.name}`).mtime.getTime()
                    }
                })
                .sort((first, last) => first.time - last.time)
                .map((directory) => directory.name);
        }
    }
}
