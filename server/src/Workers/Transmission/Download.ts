import SocketIO from "socket.io";
import path from 'path';
import fse from 'fs-extra';
import Joi from 'joi';
import CreationManager from "../FileManager/Create";
import { Categories, MEDIAPATH, TEMPLOCATION } from "../Order/Constants";
import { getRepository } from "typeorm";
import GalleryFile from "../../DatabaseModel/models/galleryfile";
import Store from "../../DatabaseModel/models/store";
import { Lifecycle, Request } from "@hapi/hapi";
import Boom from "@hapi/boom";
import http from 'http';
import https from 'https';
import contentDisposition from 'content-disposition';
import { guessFileCategory } from "../Order/Inspector";

/*
import { DownloaderHelper, Stats } from 'node-downloader-helper';
*/
export default class DownloadManager {

    private creationManager: CreationManager;
    private io: SocketIO.Server;

    private static instance: DownloadManager = null;

    /**
     * Singleton initialiser
     * @param io SocketIO Server
     */
    public static getInstance = (io: SocketIO.Server): DownloadManager => {
        if (DownloadManager.instance == null) {
            DownloadManager.instance = new DownloadManager(io);
        }
        return DownloadManager.instance;
    }

    /**
     * Please use CreationManager.getInstance() instead
     * @param io
     * @private
     */
    private constructor(io: SocketIO.Server) {
        this.io = io;
        this.creationManager = CreationManager.getInstance();
    }

    public async conductDownload(request: Request): Promise<Lifecycle.ReturnValue> {
        let httpPayload = request.payload;
        let fileUrl = httpPayload['url'];
        let storeId = parseInt(httpPayload['storeId']);
        let cookies = request.state;
        let socketId = '';
        if (cookies.token && cookies.token["token"]) {
            socketId = cookies.token["token"];
        }
        if (Joi.string().uri().validate(fileUrl)) {
            let storeRepo = getRepository(Store);
            let store = await storeRepo.findOne(storeId);
            if (store === undefined) {
                return Boom.badRequest("Unable to find the store");
            }
            let fileRepo = getRepository(GalleryFile);
            let galleryFile = fileRepo.create({
                store, storeId
            });
            // deliberately not awaiting this method
            this.downloadFile(fileUrl, galleryFile, socketId);
        }
        return true;
    }

    private notifyError(socketId: string, fileUrl: string, error: Error) {
        this.io.to(socketId).emit("spawnToast", {
            title: `Unable to download ${fileUrl}`,
            text: `${error}`,
            type: 'error'
        });
    }

    private async downloadFile(fileUrl: string, file: GalleryFile, socketId: string): Promise<void> {
        const protocol = (fileUrl.startsWith('https://')) ? https : http;
        let receievedSize = 0;
        let totalSize = 0;
        let latestPercentage = 0;
        try {
            const request = protocol.get(fileUrl);
            request.on('response', async (response) => {
                const headers = response.headers;
                totalSize = parseInt(headers['content-length']);
                const filename = headers['content-disposition']
                    ? decodeURI(
                        contentDisposition.parse(response.headers['content-disposition']).parameters.filename
                    )
                    : path.basename(new URL(fileUrl).pathname);
                let downloadedFile = path.parse(filename);
                file.name = downloadedFile.name;
                file.extension = downloadedFile.ext;
                let category = guessFileCategory(filename);
                if (category === null) {
                    this.notifyError(socketId, fileUrl, new Error("This type of file is not supported yet."));
                    request.end();
                    return;
                }
                let fileDestination = path.join(MEDIAPATH, file.store.name, category.title, downloadedFile.name, 'Raw' + downloadedFile.ext);
                if (await fse.pathExists(fileDestination)) {
                    this.notifyError(socketId, fileUrl, new Error("The file already exists."));
                    request.end();
                    return;
                }
                file.categoryId = Categories.indexOf(category);
                console.log(`Downloading: ${filename} from ${fileUrl}`);
                let tempDir = await fse.mkdtemp(TEMPLOCATION + path.sep);
                const downloadDestination = path.join(tempDir, filename);
                const writeStream = fse.createWriteStream(downloadDestination);
                response.pipe(writeStream);
                writeStream.on('error', (error) => {
                    writeStream.close();
                    request.end();
                    this.notifyError(socketId, fileUrl, error);
                });
                response.on('data', (chunk) => {
                    receievedSize += chunk.length;
                    let percentageDone = Math.ceil(receievedSize / totalSize * 100);
                    if (percentageDone % 10 === 0 && latestPercentage !== percentageDone) {
                        latestPercentage = percentageDone;
                        console.log(`${percentageDone}% done.`);
                    }
                });
                writeStream.on('finish', async () => {
                    let category = Categories[file.categoryId];
                    let fileDestination = path.join(MEDIAPATH, file.store.name, category.title, downloadedFile.name, 'Raw' + downloadedFile.ext);
                    writeStream.close();
                    await fse.move(downloadDestination, fileDestination);
                    await file.save();
                    await fse.remove(tempDir);
                    this.io.to(socketId).emit("addFile", file);
                    await this.creationManager.process(file, path.parse(fileDestination));
                });

            }).on('error', (error) => {
                request.end();
                this.notifyError(socketId, fileUrl, error);
            });
        } catch (e) {
            console.error("Denied: " + fileUrl);
            this.notifyError(socketId, fileUrl, e);
        }
    }
}
