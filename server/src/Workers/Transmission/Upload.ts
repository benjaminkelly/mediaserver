import { isReadableStream } from "../../utils/StreamHelper";
import SocketIO from 'socket.io';
import Hapi from '@hapi/hapi';
import path from 'path';
import Boom from '@hapi/boom';
import {Categories, CategoryEnum, MEDIAPATH, TEMPLOCATION} from '../Order/Constants';
import CreationManager from '../FileManager/Create';
import fse from 'fs-extra';
import {getRepository} from "typeorm";
import { v4 as uuid4 } from 'uuid';
import GalleryFile from "../../DatabaseModel/models/galleryfile";
import Store from "../../DatabaseModel/models/store";

export default class Uploader {

    private creationManager: CreationManager;
    private io: SocketIO.Server;
    private tasks = 0;

    private static instance: Uploader = null;

    /**
     * Singleton initialiser
     * @param io SocketIO Server
     */
    public static getInstance = (io: SocketIO.Server): Uploader => {
        if (Uploader.instance == null) {
            Uploader.instance = new Uploader(io);
        }
        return Uploader.instance;
    }

    /**
     * Please use Uploader.getInstance() instead
     * @param io SocketIO Server
     * @private
     */
    private constructor(io: SocketIO.Server) {
        this.io = io;
        this.creationManager = CreationManager.getInstance();
        this.creationManager.on("progress", (percent, file) => {
            this.io.to(file.socketId).emit("progress", {id: file.processId, percent, name: file.name});
        });
        this.creationManager.on("uploadFinished", async (file) => {
            await getRepository(GalleryFile).save(file);
            this.io.to(`store-${file.store.id}`).emit("addFile", file);
        });
        this.creationManager.on("finished", (file) => {
            let category = Categories[file.categoryId];
            if (category.title === CategoryEnum.VIDEOS) {
                this.io.to(`store-${file.store.id}`).emit('gifReady', file);
            }
        });
    }

    private async shouldThrottle() {
        if (this.tasks >= 1) {
            this.io.emit('serverBusy', "");
            return true;
        }
        else {
            return false;
        }
    }

    public async conductUpload(request: Hapi.Request) {
        return new Promise((resolve) => {
            let payload = request.payload;
            if (!isReadableStream(payload["file"])) {
                resolve(Boom.preconditionRequired());
            }
            let signOfLifeDelay = 0;
            let throttle = setInterval(async () => {
                signOfLifeDelay += 500;
                if (await this.shouldThrottle() === false) {
                    this.tasks += 1;
                    let result = await this.process(request);
                    this.tasks -= 1;
                    resolve(result);
                    clearInterval(throttle);
                }
                else if (signOfLifeDelay > 30000) {
                    console.log("Server is busy: please wait");
                    signOfLifeDelay = 0;
                }
            }, 500);
        })
    }

    private async process(request: Hapi.Request) {
        const payload = request.payload;
        let cookies = request.state;
        let socketId = '';
        if (cookies.token && cookies.token["token"]) {
            socketId = cookies.token["token"];
        }
        const storeId = payload["storeId"];
        const incomingFile = payload["file"];
        const categoryId = parseInt(request.pre["category"]);

        let storeRepo = getRepository(Store);
        let store = await storeRepo.findOne(storeId);
        if (store === undefined) {
            return Boom.badData("The given storeID does not exist");
        }
        let category = Categories[categoryId];

        const filename = incomingFile.hapi.filename;
        await fse.ensureDir(TEMPLOCATION);

        let tempFilePath = path.join(TEMPLOCATION, filename);

        let fileStream = fse.createWriteStream(tempFilePath, { autoClose: true });
        incomingFile.pipe(fileStream);
        return new Promise((resolve) => {
            fileStream.on('finish', async () => {
                let fileProfile = path.parse(filename);
                let filePackageName = fileProfile.name;
                let fileExtension = fileProfile.ext.toLowerCase();

                let storeLocation = path.join(MEDIAPATH, store.name);
                let tempFile = path.join(TEMPLOCATION, filename);

                let destinationFile: path.ParsedPath = path.parse(path.join(path.join(storeLocation, category.title, filePackageName), `Raw${fileExtension}`));

                let packageName = path.parse(filename).name;

                let fileExists = await fse.pathExists(path.format(destinationFile));

                if (fileExists === false) {
                    await fse.ensureDir(destinationFile.dir);
                } else {
                    let incomingFileStats = await fse.lstat(tempFile);
                    let incomingFileSize = incomingFileStats.size;
                    let storedFileStats = await fse.lstat(path.format(destinationFile));
                    let storedFileSize = storedFileStats.size;

                    if (incomingFileSize === storedFileSize) {
                        await fse.remove(tempFile);
                        console.log(`It's a duplicate`);
                        return resolve(Boom.conflict(`${filename} exists already on the server.`));
                    } else {
                        let cycle = 0;
                        do {
                            cycle++;
                        } while (await fse.pathExists(path.join(storeLocation, category.title, `${packageName}-${cycle}`)));
                        destinationFile.dir = path.join(storeLocation, category.title, `${packageName}-${cycle}`);
                        destinationFile.name = "Raw";
                    }
                }
                await fse.move(tempFile, path.format(destinationFile));

                let fileRepo = getRepository(GalleryFile);
                let galleryFile = fileRepo.create({
                    name: fileProfile.name,
                    categoryId,
                    store,
                    extension: destinationFile.ext
                });
                galleryFile.socketId = socketId;
                galleryFile.processId = uuid4();
                this.creationManager.once("progressBegin", (file) => {
                    resolve(file);
                });
                this.creationManager.once("uploadFinished", (file) => {
                    resolve(file);
                });
                let response = await this.creationManager.process(galleryFile, destinationFile).catch((error) => {
                    console.error(error);
                    return resolve(Boom.internal());
                });
                if (this.io.sockets.sockets.get(socketId)) {
                    this.io.sockets.sockets.get(socketId).emit('serverIdle', "");
                }
                // Let the first client get first dibs before others
                setTimeout(() => {
                    this.io.emit('serverIdle', "");
                }, 2000);
                resolve(response);
            });
        });
    }
}
