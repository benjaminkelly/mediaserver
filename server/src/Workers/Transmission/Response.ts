export function greet(username, issuedToken) {
        return {
            code: 200,
            comment: 'Welcome ' + username,
            data: {
                token: issuedToken
            }
        };
    }
    export function farewell(username) {
        return {
            code: 200,
            comment: 'Farewell ' + username
        };
    }
    export function insufficientRequirements() {
        return {
        code: 500,
        comment: 'Insufficient requirements fulfilled.'
    };
}

export function noFile() {
    return {
        code: 404,
        comment: 'This file does no exist.'
    };
}

export function invalidToken() {
    return {
        code: 403,
        comment: 'This token isn\'t associated with a user.'
    };
}

export function invalidCredentials() {
    return {
        code: 403,
        comment: 'This password does not fit your user.'
    };
}

export function catastrophicCalamity() {
    return {
        code: 503,
        comment: 'An unfortunate complication has occurred, you have been logged out.'
    };
}

export function listStores(stores) {
    return {
        code: 200,
        comment: 'Associated stores found.',
        data: {
            stores: stores
        }
    };
}

export function giveFetched(data) {
    return {
        code: 200,
        comment: 'fetched',
        data: data
    };
}
