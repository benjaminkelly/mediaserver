import {Categories, Category} from "./Constants";
import path from "path";

export function guessFileCategory(file: string): Category {
    file = path.parse(file).base;
    let result: Category = null;
    for (let category of Categories) {
            let regexMatch = new RegExp("^[^\.][^\\\/]+\." + category.extensions, "ig");
            if (file.match(regexMatch)) {
                result = category;
                break;
            }
    }
    return result;
}
