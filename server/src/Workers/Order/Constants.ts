import path from 'path';

export enum FileNameEnum {
    preview = "Preview",
    raw = "Raw",
    thumb = "Thumb"
}

export enum RedisPrefixes {
    cookiePw = 'mediaserver-cookiePW'
}

export function buildFilePath(store, category, file) {
    return path.join(MEDIAPATH, store, category, file);
}

export const PUBLICDIR = path.join(__dirname, "../../../", "public");

export const MEDIAPATH = path.join(PUBLICDIR, "media");
export const TEMPLOCATION = path.join(PUBLICDIR, "temp");

export enum CategoryEnum {
    IMAGES = 'images', VIDEOS = 'videos'
}

export class Category {
    title: CategoryEnum;
    extensions: string;
    convertTo?: string;
    acceptedExts?: string[];
    hideTitle: boolean;
    htmlContainer: string;
    constructor(title: CategoryEnum, extensions: string, acceptedExts: string[], convertTo: string, hideTitle: boolean, htmlContainer: string) {
        this.title = title;
        this.extensions = extensions;
        this.acceptedExts = acceptedExts;
        this.convertTo = convertTo;
        this.hideTitle = hideTitle;
        this.htmlContainer = htmlContainer;
    }
}

export const Categories: Readonly<Category[]> = [
    new Category(CategoryEnum.IMAGES, "(gif|jpg|png|jpeg)", null,null, true, "img-container"),
    new Category(CategoryEnum.VIDEOS, "(mpeg|mp4|avi|webm|wmv|mov|mkv|ts|ogg)", [".mp4", ".ogg"], '.mp4', false, "vid-container")
];

export const CategoryTitles: string[] = Categories.map((category: Category) => category.title);

