import { Server } from "socket.io";
import UserHandler from '../Security/UserHandler';


export default class SocketBinder {

    static init(io: Server) {

        io.use(async (socket, next) => {
            let token = await UserHandler.extractTokenFromCookies(socket.handshake.headers.cookie);
            if (token) {
                let identity = await UserHandler.proveIdentity(null, token);
                if (identity.isValid && identity.credentials.storeIds) {
                    await socket.join(identity.credentials.storeIds.map((storeId) => `store-${storeId}`));
                    await socket.join(token);
                }
                else {
                    socket.disconnect();
                }
            }
            socket.on('leave', async () => {
                socket.disconnect();
            });
            next();
        });
    }
}

