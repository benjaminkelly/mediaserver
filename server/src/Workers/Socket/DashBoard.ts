import Realm from "../../DatabaseModel/models/realm";
import SocketIO from "socket.io";
import {diskinfo} from '@dropb/diskinfo';
import Gatherer from "../FileManager/Gather";
import {getRepository} from "typeorm";
import path from "path";
import User from "../../DatabaseModel/models/user";
import RealmHasStore from "../../DatabaseModel/models/realmhasstore";

export default class DashBoardManager {

    public static init(io: SocketIO.Server) {

        const gatherer = new Gatherer();
        io.use((socket, next) => {
            socket.on("getDiskCapacity", (session) => {
                diskinfo(path.join("./public", "media")).then((diskProfile) => {
                    let used = Math.round(diskProfile.used / 1000000000 * 100) / 100;
                    let free = Math.round(diskProfile.avail / 1000000000 * 100) / 100;
                    let total = Math.round(diskProfile.size / 1000000000 * 100) / 100;
                    session.emit("diskCapacityResult", {free: free, used: used, total: total});
                }).catch((error) => {
                    next(error);
                    return console.error(error);
                });
            });
            socket.on("getRealms", async () => {
                let realmQuery = await getRepository(Realm).find({select: ["id", "name"]});
                let realms = [];
                realmQuery.forEach((realm) => {
                    realms.push({
                        id: realm.id,
                        name: realm.name
                    });
                });
                socket.emit("realmResult", {realms: realms});
            });

            socket.on("getMediaMetrics", async () => {
                const metrics = await gatherer.countMediaFiles();
                socket.emit("mediaMetricsResult", {metrics: metrics});
            });

            socket.on("switchRealm", async (options) => {
                if (options.userId != null && options.realmId != null) {
                    let userRepo = getRepository(User);
                    let realmRepo = getRepository(Realm);
                    let userId = options.userId;
                    let realmId = options.realmId;
                    let user = await userRepo.findOne({where: {id: userId}});
                    if (user) {
                        let realm = await realmRepo.findOne({where: {id: realmId}});
                        if (realm) {
                            await userRepo.update(user, {realm: realm});
                        }
                    }
                }
            });

            socket.on("linkStore", async (options) => {
                if (options.realmId != null && options.storeId != null) {
                    let realmHasStoreRepo = getRepository(RealmHasStore);
                    let realmId = options.realmId;
                    let storeId = options.storeId;
                    let realmStoreAssociation = await realmHasStoreRepo.findOne({
                        realmId: realmId,
                        storeId: storeId
                    });
                    if (realmStoreAssociation == null) {
                        await realmHasStoreRepo.save({
                            realmId: realmId,
                            storeId: storeId
                        });
                    }
                }
            });

            socket.on("severStoreLink", async (options) => {
                if (options.realmId != null && options.storeId != null) {
                    let realmId: number = options.realmId;
                    let storeId: number = options.storeId;
                    let realmHasStoreRepo = getRepository(RealmHasStore);
                    let association = await realmHasStoreRepo.findOne({
                        where: {
                            realmId: realmId,
                            storeId: storeId
                        }
                    });
                    if (association) {
                        await realmHasStoreRepo.remove([association]);
                    }
                }
            });
            next();
        });
    }
}
