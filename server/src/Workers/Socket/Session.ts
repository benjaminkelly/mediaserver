import SocketIO from "socket.io";
import IORedis from "ioredis";

export default class SessionManager {

    public static init(io: SocketIO.Server, redisClient: IORedis.Redis) {

        io.use((socket, next) => {

            socket.on("isServerBusy",
                async () => {
                    let inProgress = await redisClient.get('inProgress');
                    if (inProgress === 'true') {
                        socket.emit('serverBusy', "");
                    } else if (inProgress === 'false') {
                        // Covered by delay after upload
                    } else {
                        await redisClient.set('inProgress', 'false');
                    }
                });

            socket.on("rooms", () => {
                let rooms = new Array();
                for (const room of socket.rooms) {
                    rooms.push(room);
                }
                socket.send(rooms);
            });
            next();
        });
    }
}
