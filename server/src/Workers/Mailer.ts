import nodemailer from 'nodemailer';

export async function sendVerification(name: string, recipient: string, link: string): Promise<void> {
    let testAccount = await nodemailer.createTestAccount();

    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass // generated etheeal password
        }
    });

    let info = await transporter.sendMail({
        from: '"RasPi 👻" <foo@example.com>', // sender address
        to: recipient, // list of receivers
        subject: "E-Mail Verification", // Subject line
        text: `Hello ${name}, please open this link to verify your gallery account \n${link}`, // plain text body
        html: `<p>Hello <strong><i>${ name }</i></strong>, please open this link to verify your gallery account<br><a href="${link}">${link}</a></p>` // html body
    });

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
