import glob from 'fast-glob';
import {Categories, Category, MEDIAPATH} from "../Order/Constants";
import path from "path";
import fse from 'fs-extra';
import {guessFileCategory} from "../Order/Inspector";
import CreationManager from "../FileManager/Create";
import { red } from 'colors/safe';
import slash from "../../utils/slash";
import {getRepository} from "typeorm";
import GalleryFile from "../../DatabaseModel/models/galleryfile";
import Store from "../../DatabaseModel/models/store";

export default async function cleanUpMediaDir() {
    console.log("Scanning for uncategorised and unfinished files...");
    let storeGlob = slash(path.posix.join(MEDIAPATH, '/*'));
    let storesStream = glob.stream(storeGlob, {onlyDirectories: true});

    for await (const storePath of storesStream) {
        // List all files within the store path without a category
        let rougeFilesStream = glob.stream(storePath.toString() + '/*', {onlyFiles: true});
        await processUnfinishedFiles(rougeFilesStream, storePath.toString());
        // List all files within the category paths
        let categoriesStream = glob.stream(storePath.toString() + '/*', {onlyDirectories: true});
        for await (const categoryPath of categoriesStream) {
            let categoryName = path.parse(categoryPath.toString()).base;
            let filePackagesStream = glob.stream(categoryPath.toString() + '/*/Raw.*', {onlyFiles: true});
            await processUnfinishedFiles(filePackagesStream, null, categoryName);
        }
    }
}

async function processUnfinishedFiles(filesStream: NodeJS.ReadableStream, storePath: string = null, categoryName: string = null) {
    let storeName;
    let category: Category;
    for await (const filePath of filesStream) {
        let fileProperties = path.parse(filePath.toString());
        if (categoryName != null) {
            category = Categories.find((category) => category.title === categoryName);
        } else {
            category = guessFileCategory(fileProperties.base);
        }
        if (category != null) {
            let fileDestination = { ...fileProperties };
            // If storePath is assigned then it must be moved
            if (storePath != null) {
                storeName = path.parse(storePath.toString()).base;
                fileDestination = path.parse(path.join(storePath, category.title, fileProperties.name, '/Raw' + fileProperties.ext));
                await fse.move(path.format(fileProperties), path.format(fileDestination));
            }
            // Otherwise it is located in {fileDestination} = {storePath}/{categoryName}/{file}
            else {
                storeName = path.basename(path.resolve(fileDestination.dir, '../../'));
            }

            let storeRepo = getRepository(Store);
            let store = await storeRepo.findOne({
                where: {
                    name: storeName
                }
            });

            let creationManager = CreationManager.getInstance();

            let fileRepo = getRepository(GalleryFile);
            let file: GalleryFile = fileRepo.create({
                name: path.basename(fileDestination.dir),
                categoryId: Categories.indexOf(category),
                extension: fileDestination.ext
            });
            if (store) {
                file.store = store;
            }

            await creationManager.process(file, fileDestination).then(async (file) => {
                // if file does not exist in db
                if (await fileRepo.count({ where: { name: file.name } }) === 0) {
                    let storeRepo = getRepository(Store);
                    let store = await storeRepo.findOne({
                        where: {
                            name: storeName
                        },
                        cache: true
                    });
                    if (store !== undefined) {
                        await fileRepo.save(file);
                    }
                }
            }).catch((error) => {
                console.error(red(error));
            });
        }
    }
}
